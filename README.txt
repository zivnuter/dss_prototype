Direct Sales System
===================

Zdrojovy kod prototypu se nachazi ve slozce src/impl/dss_prototype
Ke spravnemu spusteni software potrebujete:

- zahrnout do projektu vsechny knihovny ze slozky src/impl/libs/*
- JDK 9
- vytvorit databazi z modelu nebo prilozeneho scriptu ve slozce src/impl/database/*
  (model je vytvareny v nastroji MySQL Workbench a pomoci Database->Forward Engineer lze databaze z modelu vytvorit)
- nastavit si ve tride GeneralConstants pristup k lokalni databazi (konstanty JDBC_URL, JDBC_USER a JDBC_PASS)
  (cesta k souboru: src/impl/dss_prototype/src/main/constants/GeneralConstants.java)
- spustit aplikaci a vytvorit ucet -> to inicializuje databazi
- spustit soubor src/impl/data/create_data.sql -> vytvori zakladni data v aplikaci