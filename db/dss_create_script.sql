-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema dss_prototype
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dss_prototype
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dss_prototype` ;
USE `dss_prototype` ;

-- -----------------------------------------------------
-- Table `dss_prototype`.`zakaznik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`zakaznik` (
  `zakaznik_id` INT NOT NULL AUTO_INCREMENT,
  `jmeno` VARCHAR(45) NOT NULL,
  `prijmeni` VARCHAR(45) NOT NULL,
  `telefon` VARCHAR(45) NULL,
  `poznamky` VARCHAR(200) NULL,
  PRIMARY KEY (`zakaznik_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`atribut_zakaznika`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`atribut_zakaznika` (
  `atribut_id` INT NOT NULL AUTO_INCREMENT,
  `nazev` VARCHAR(45) NOT NULL,
  `typ_pole` VARCHAR(45) NOT NULL,
  `povinne` VARCHAR(45) NOT NULL,
  `poradi` INT NOT NULL,
  PRIMARY KEY (`atribut_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`hodnota`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`hodnota` (
  `hodnota_id` INT NOT NULL AUTO_INCREMENT,
  `zakaznik_id` INT NOT NULL,
  `atribut_id` INT NOT NULL,
  `hodnota` VARCHAR(45) NOT NULL,
  `poradi` VARCHAR(45) NOT NULL,
  INDEX `fk_zakaznik_has_atribut_zakaznika_atribut_zakaznika1_idx` (`atribut_id` ASC),
  INDEX `fk_zakaznik_has_atribut_zakaznika_zakaznik_idx` (`zakaznik_id` ASC),
  PRIMARY KEY (`hodnota_id`),
  CONSTRAINT `fk_zakaznik_has_atribut_zakaznika_zakaznik`
    FOREIGN KEY (`zakaznik_id`)
    REFERENCES `dss_prototype`.`zakaznik` (`zakaznik_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_zakaznik_has_atribut_zakaznika_atribut_zakaznika1`
    FOREIGN KEY (`atribut_id`)
    REFERENCES `dss_prototype`.`atribut_zakaznika` (`atribut_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`duvod_k_upomince`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`duvod_k_upomince` (
  `duvod_id` INT NOT NULL AUTO_INCREMENT,
  `nazev` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`duvod_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`upominka`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`upominka` (
  `upominka_id` INT NOT NULL AUTO_INCREMENT,
  `zakaznik_id` INT NOT NULL,
  `duvod_id` INT NOT NULL,
  `datum` DATE NOT NULL,
  `poznamky` VARCHAR(200) NULL,
  PRIMARY KEY (`upominka_id`),
  INDEX `fk_upominka_zakaznik1_idx` (`zakaznik_id` ASC),
  INDEX `fk_upominka_duvod_k_upomince1_idx` (`duvod_id` ASC),
  CONSTRAINT `fk_upominka_zakaznik1`
    FOREIGN KEY (`zakaznik_id`)
    REFERENCES `dss_prototype`.`zakaznik` (`zakaznik_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_upominka_duvod_k_upomince1`
    FOREIGN KEY (`duvod_id`)
    REFERENCES `dss_prototype`.`duvod_k_upomince` (`duvod_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`typ_korespondence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`typ_korespondence` (
  `typ_id` INT NOT NULL AUTO_INCREMENT,
  `nazev` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`typ_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`korespondence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`korespondence` (
  `koredpondence_id` INT NOT NULL AUTO_INCREMENT,
  `zakaznik_id` INT NOT NULL,
  `typ_id` INT NOT NULL,
  `datum a cas` DATETIME NOT NULL,
  `duvod` VARCHAR(45) NULL,
  `zareagoval(a)` VARCHAR(45) NULL,
  `poznamky` VARCHAR(200) NULL,
  `korespondencecol` VARCHAR(45) NULL,
  PRIMARY KEY (`koredpondence_id`),
  INDEX `fk_korespondence_zakaznik1_idx` (`zakaznik_id` ASC),
  INDEX `fk_korespondence_typ_korespondence1_idx` (`typ_id` ASC),
  CONSTRAINT `fk_korespondence_zakaznik1`
    FOREIGN KEY (`zakaznik_id`)
    REFERENCES `dss_prototype`.`zakaznik` (`zakaznik_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_korespondence_typ_korespondence1`
    FOREIGN KEY (`typ_id`)
    REFERENCES `dss_prototype`.`typ_korespondence` (`typ_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`udalost`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`udalost` (
  `udalost_id` INT NOT NULL AUTO_INCREMENT,
  `popis` VARCHAR(45) NOT NULL,
  `typ` VARCHAR(45) NOT NULL,
  `datum_a_cas` DATETIME NOT NULL,
  `kapacita` VARCHAR(45) NULL,
  `poznamky` VARCHAR(200) NULL,
  PRIMARY KEY (`udalost_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`ucast`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`ucast` (
  `zakaznik_id` INT NOT NULL,
  `udalost_id` INT NOT NULL,
  `potvrzeno` VARCHAR(45) NULL,
  PRIMARY KEY (`zakaznik_id`, `udalost_id`),
  INDEX `fk_ucast_jina_udalost1_idx` (`udalost_id` ASC),
  CONSTRAINT `fk_ucast_zakaznik1`
    FOREIGN KEY (`zakaznik_id`)
    REFERENCES `dss_prototype`.`zakaznik` (`zakaznik_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ucast_jina_udalost1`
    FOREIGN KEY (`udalost_id`)
    REFERENCES `dss_prototype`.`udalost` (`udalost_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`objednavka_zakaznika`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`objednavka_zakaznika` (
  `objednavka_id` INT NOT NULL AUTO_INCREMENT,
  `stav` VARCHAR(45) NOT NULL,
  `cena` DOUBLE NOT NULL,
  `datum_doruceni` DATE NULL,
  `poznamky` VARCHAR(200) NULL,
  `zakaznik_id` INT NOT NULL,
  PRIMARY KEY (`objednavka_id`),
  INDEX `fk_objednavka_zakaznika_zakaznik1_idx` (`zakaznik_id` ASC),
  CONSTRAINT `fk_objednavka_zakaznika_zakaznik1`
    FOREIGN KEY (`zakaznik_id`)
    REFERENCES `dss_prototype`.`zakaznik` (`zakaznik_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`produkt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`produkt` (
  `produkt_id` INT NOT NULL AUTO_INCREMENT,
  `nazev` VARCHAR(45) NOT NULL,
  `cena` DOUBLE NOT NULL,
  `pocet_ks` INT NOT NULL,
  `prumerna_vydrz` DOUBLE NULL,
  `poznamky` VARCHAR(200) NULL,
  PRIMARY KEY (`produkt_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`polozka_objednavky`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`polozka_objednavky` (
  `polozka_obj_id` INT NOT NULL AUTO_INCREMENT,
  `pocet_ks` VARCHAR(45) NOT NULL,
  `nazev_produktu` VARCHAR(45) NOT NULL,
  `cena` DOUBLE NOT NULL,
  `prumerna_vydrz` DOUBLE NULL,
  `objednavka_id` INT NOT NULL,
  `produkt_id` INT NULL,
  PRIMARY KEY (`polozka_obj_id`),
  INDEX `fk_polozka_objednavky_objednavka_zakaznika1_idx` (`objednavka_id` ASC),
  INDEX `fk_polozka_objednavky_produkt1_idx` (`produkt_id` ASC),
  CONSTRAINT `fk_polozka_objednavky_objednavka_zakaznika1`
    FOREIGN KEY (`objednavka_id`)
    REFERENCES `dss_prototype`.`objednavka_zakaznika` (`objednavka_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_polozka_objednavky_produkt1`
    FOREIGN KEY (`produkt_id`)
    REFERENCES `dss_prototype`.`produkt` (`produkt_id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`sklad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`sklad` (
  `sklad_id` INT NOT NULL AUTO_INCREMENT,
  `nazev` VARCHAR(45) NOT NULL,
  `je_defaultni` INT NOT NULL,
  PRIMARY KEY (`sklad_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`naskladneni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`naskladneni` (
  `naskladneni_id` INT NOT NULL AUTO_INCREMENT,
  `typ` VARCHAR(45) NOT NULL,
  `datum objednani` DATE NOT NULL,
  `datum dodani` DATE NULL,
  `cislo_objednavky` VARCHAR(45) NULL,
  `poznamky` VARCHAR(200) NULL,
  `sklad_id` INT NOT NULL,
  PRIMARY KEY (`naskladneni_id`),
  INDEX `fk_naskladneni_sklad1_idx` (`sklad_id` ASC),
  CONSTRAINT `fk_naskladneni_sklad1`
    FOREIGN KEY (`sklad_id`)
    REFERENCES `dss_prototype`.`sklad` (`sklad_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`polozka_naskladneni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`polozka_naskladneni` (
  `polozka_naskl_id` INT NOT NULL AUTO_INCREMENT,
  `nazev` VARCHAR(45) NOT NULL,
  `cena` DOUBLE NOT NULL,
  `pocet_ks` INT NOT NULL,
  `produkt_id` INT NULL,
  `naskladneni_id` INT NOT NULL,
  PRIMARY KEY (`polozka_naskl_id`),
  INDEX `fk_polozka_naskladneni_produkt1_idx` (`produkt_id` ASC),
  INDEX `fk_polozka_naskladneni_naskladneni1_idx` (`naskladneni_id` ASC),
  CONSTRAINT `fk_polozka_naskladneni_produkt1`
    FOREIGN KEY (`produkt_id`)
    REFERENCES `dss_prototype`.`produkt` (`produkt_id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_polozka_naskladneni_naskladneni1`
    FOREIGN KEY (`naskladneni_id`)
    REFERENCES `dss_prototype`.`naskladneni` (`naskladneni_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`prihlaseni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`prihlaseni` (
  `prihlaseni_id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `heslo` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`prihlaseni_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`typ_nastaveni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`typ_nastaveni` (
  `typ_id` INT NOT NULL AUTO_INCREMENT,
  `nazev` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`typ_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dss_prototype`.`funkce_aplikace`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dss_prototype`.`funkce_aplikace` (
  `funkce_id` INT NOT NULL,
  `nazev` VARCHAR(45) NOT NULL,
  `zapnuto` INT NOT NULL,
  `typ_nastaveni_typ_id` INT NOT NULL,
  PRIMARY KEY (`funkce_id`),
  INDEX `fk_funkce_aplikace_typ_nastaveni1_idx` (`typ_nastaveni_typ_id` ASC),
  CONSTRAINT `fk_funkce_aplikace_typ_nastaveni1`
    FOREIGN KEY (`typ_nastaveni_typ_id`)
    REFERENCES `dss_prototype`.`typ_nastaveni` (`typ_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
