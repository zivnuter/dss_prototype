package main.constants;

public class DialogContants {
    //GENERAL
    public static final String NOT_IMPLEMENTED = "Funkce prozatím nebyla implementována.";
    public static final String DIALOG_FILL_MANDATORY = "Nejsou vyplněna všechna povinná pole.\n" +
            "Vyplňte prosím pole označené '*' a akci opakujte.";
    public static final String DIALOG_FILL_NUMBERS = "Některá pole vyžadují číselnou hodnotu.";
    public static final String DIALOG_NO_CHANGES = "Nebyly provedeny žádné změny.";
    public static final String DIALOG_ERROR = "Nastala chyba. Zkontrolujte Vámi zadaná data a opakujte.";
    public static final String DIALOG_UPDATE_OK = "Změny byly úspěšně uloženy.";
    public static final String DIALOG_UNDERSTAND = "Rozumím";
    public static final String DIALOG_YES = "Ano";
    public static final String DIALOG_NO = "Ne";

    //LOGIN
    public static final String LOGIN_DIALOG_WARNING = "Zadal(a) jste špatný e-mail nebo heslo.";
    public static final String LOGIN_DIALOG_CREATE_MANDATORY = "Musíte vyplnit e-mail i heslo.";
    public static final String LOGIN_DIALOG_CREATE_EMAIL = "Zadejte prosím e-mail ve správném formátu.";
    public static final String LOGIN_DIALOG_CREATE_OK = "Účet byl úspěšně vytvořen.\nNyní se prosím přihlašte.";

    //CUSTOMER
    public static final String CUSTOMER_CREATE_DIALOG_TROW = "Opravdu si přejete zahodit vytvořené změny?";
    public static final String CUSTOMER_CREATE_DIALOG_CREATED = "Zákazník byl úspěšně vytvořen.";
    public static final String CUSTOMER_DELETE_DIALOG = "Opravdu si přejete tohoto zákazníka nenávratně smazat?";
    public static final String CUSTOMER_DELETE_DIALOG_OK = "Zákazník byl úspěšně smazán.";
    public static final String CUSTOMER_UPDATE_DIALOG = "Opravdu si přejete uložit změny u tohoto zákazníka?";
    public static final String CUSTOMER_UPDATE_DIALOG_NO_CHANGES = "Nebyly provedeny žádné změny.";

    //STORE
    public static final String STORE_ERROR = "Nepodařilo se načíst sklad.\nAkci opakujte nebo se obraťte na vývojáře.";
    public static final String STORE_DELETE_DIALOG = "Opravdu si přejete tento produkt nenávratně smazat?";
    public static final String STORE_DELETE_DIALOG_OK = "Produkt byl úspěšně smazán.";
    public static final String STORE_UPDATE_DIALOG = "Opravdu si přejete uložit změny u tohoto produktu?";
    public static final String STORE_CREATE_DIALOG_THROW = "Opravdu si přejete zahodit vytvořené změny?.";
    public static final String STORE_CREATE_DIALOG_OK = "Produkt byl úspěšně vytvořen.";

    //ORDERS
    public static final String NO_PRODUCTS = "Nepřidal(a) jste žádné produkty.\nMusí být v seznamu alespoň jeden.";
    public static final String PRODUCTS_MANDATORY = "U jednotlivých produktů musíte vyplnit všechna pole a správně.";
    public static final String ORDER_CREATE_OK = "Objednávka zákazníka byla v pořádku vytvořena.";
    public static final String ORDER_DELETE_OK = "Objednávka zákazníka byla úspěšně odebrána.";
    public static final String ORDER_DELETE_PROMPT = "Opravdu si objednávku nenávratně smazat?";

    //REMINDERS
    public static final String PROBLEM_WITH_REASONS = "Nepodařilo se načíst důvody k upomínce.";
    public static final String REMINDERS_CREATE_OK = "Upomínka byla úspěšně vytvořena.";
    public static final String REMINDERS_CREATE_DELETE = "Opravdu si přejete tuto upomínku smazat?";
    public static final String REMINDERS_CREATE_DELETE_OK = "Upomínka byla úspěšně smazána.";
}
