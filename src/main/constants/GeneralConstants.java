package main.constants;

public class GeneralConstants {
    public static final String STRING_TRUE = "~true";
    public static final String STRING_FALSE = "~false";

    //DB PROPERTIES
    public static final String JDBC_URL = "jdbc:mysql://localhost:3306/dss_prototype?autoReconnect=true&useSSL=false";
    public static final String JDBC_USER = "dss";
    public static final String JDBC_PASS = "dss";

    //APP_PROPERTIES
    public static final int LOGIN_HEIGHT = 600;
    public static final int LOGIN_WIDTH = 800;
    public static final String TITLE = "Direct Sales System";
}
