package main.constants;

public class SQLCommands {
    public static final String INSERT = "INSERT INTO %s(%s) VALUES(%s) ";

    public static final String DELETE = "DELETE FROM %s ";

    public static final String UPDATE = "UPDATE %s SET ";

    public static final String UPDATE_COLUMN = "%s='%s' ";

    public static final String SELECT = "SELECT %s ";

    public static final String FROM = "FROM %s ";

    public static final String LEFT_JOIN = "LEFT JOIN %s ON %s.%s=%s.%s ";

    public static final String WHERE = "WHERE %s='%s' ";

    public static final String WHERE_NOT = "WHERE %s<>'%s' ";
}
