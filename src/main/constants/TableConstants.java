package main.constants;

public class TableConstants {
    //TABLE 'PRIHLASENI'
    public final static String TABLE_LOGIN = "prihlaseni";
    public final static String TABLE_LOGIN_ID = "prihlaseni_id";
    public final static String TABLE_LOGIN_EMAIL = "email";
    public final static String TABLE_LOGIN_PASSWORD = "heslo";

    //TABLE 'ZAKAZNIK'
    public final static String TABLE_CUSTOMER = "zakaznik";
    public final static String TABLE_CUSTOMER_PK = "zakaznik_id";
    public final static String TABLE_CUSTOMER_FIRST_NAME = "jmeno";
    public final static String TABLE_CUSTOMER_SECOND_NAME = "prijmeni";
    public final static String TABLE_CUSTOMER_PHONE = "telefon";
    public final static String TABLE_CUSTOMER_NOTES = "poznamky";

    //TABLE 'VALUE'
    public final static String TABLE_VALUE = "hodnota";
    public final static String TABLE_VALUE_PK = "hodnota_id";
    public final static String TABLE_VALUE_FK_CUSTOMER = "zakaznik_id";
    public final static String TABLE_VALUE_FK_ATTRIBUTE = "atribut_id";
    public final static String TABLE_VALUE_VALUE = "hodnota";
    public final static String TABLE_VALUE_ORDER = "poradi";

    //TABLE 'CUSTOMER'S ATTRIBUTE'
    public final static String TABLE_ATTRIBUTE = "atribut_zakaznika";
    public final static String TABLE_ATTRIBUTE_PK = "atribut_id";
    public final static String TABLE_ATTRIBUTE_NAME = "nazev";
    public final static String TABLE_ATTRIBUTE_TYPE = "typ_pole";
    public final static String TABLE_ATTRIBUTE_MANDATORY = "povinne";
    public final static String TABLE_ATTRIBUTE_ORDER = "poradi";

    //TABLE 'STORE'
    public final static String TABLE_STORE = "sklad";
    public final static String TABLE_STORE_PK = "sklad_id";
    public final static String TABLE_STORE_NAME = "nazev";
    public final static String TABLE_STORE_DEFAULT = "je_defaultni";

    //TABLE 'PRODUCT'
    public final static String TABLE_PRODUCT = "produkt";
    public final static String TABLE_PRODUCT_PK = "produkt_id";
    public final static String TABLE_PRODUCT_NAME = "nazev";
    public final static String TABLE_PRODUCT_PRICE = "cena";
    public final static String TABLE_PRODUCT_COUNT = "pocet_ks";
    public final static String TABLE_PRODUCT_ENDURANCE = "prumerna_vydrz";
    public final static String TABLE_PRODUCT_NOTES = "poznamky";

    //TABLE 'ORDER'
    public final static String TABLE_ORDER = "objednavka_zakaznika";
    public final static String TABLE_ORDER_PK = "objednavka_id";
    public final static String TABLE_ORDER_STATUS = "stav";
    public final static String TABLE_ORDER_PRICE = "cena";
    public final static String TABLE_ORDER_DATE= "datum_doruceni";
    public final static String TABLE_ORDER_NOTES = "poznamky";
    public final static String TABLE_ORDER_FK_CUSTOMER = "zakaznik_id";

    //TABLE 'ORDER PART'
    public final static String TABLE_ORDERPART = "polozka_objednavky";
    public final static String TABLE_ORDERPART_PK = "polozka_obj_id";
    public final static String TABLE_ORDERPART_COUNT = "pocet_ks";
    public final static String TABLE_ORDERPART_NAME = "nazev_produktu";
    public final static String TABLE_ORDERPART_PRICE = "cena";
    public final static String TABLE_ORDERPART_ENDURANCE = "prumerna_vydrz";
    public final static String TABLE_ORDERPART_FK_ORDER = "objednavka_id";
    public final static String TABLE_ORDERPART_FK_PRODUCT = "produkt_id";

    //TABLE 'REMINDERS'
    public final static String TABLE_REMINDERS = "upominka";
    public final static String TABLE_REMINDERS_PK = "upominka_id";
    public final static String TABLE_REMINDERS_FK_CUSTOMER = "zakaznik_id";
    public final static String TABLE_REMINDERS_FK_REASON = "duvod_id";
    public final static String TABLE_REMINDERS_DATE = "datum";
    public final static String TABLE_REMINDERS_NOTES = "poznamky";


    //TABLE 'REMINDER REASON'
    public final static String TABLE_REASON = "duvod_k_upomince";
    public final static String TABLE_REASON_PK = "duvod_id";
    public final static String TABLE_REASON_NAME = "nazev";
}
