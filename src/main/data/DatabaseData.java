package main.data;

import main.data.handle.*;

import java.sql.*;

import static main.constants.GeneralConstants.*;

public class DatabaseData {
    //HANDLE CLASSES====================================================================================================
    public HandleLogin handleLogin;
    public HandleCustomer handleCustomer;
    public HandleCustomersAttribute handleCustomersAttribute;
    public HandleCustomersValue handleCustomersValue;
    public HandleStore handleStore;
    public HandleProduct handleProduct;
    public HandleOrder handleOrder;
    public HandleOrderPart handleOrderPart;
    public HandleReminder handleReminder;
    public HandleReason handleReason;
    //==================================================================================================================

    private Connection con;
    private static DatabaseData data = new DatabaseData();

    private DatabaseData() {
        createConnection();
        handleLogin = new HandleLogin(con);
        handleCustomer = new HandleCustomer(con);
        handleCustomersAttribute = new HandleCustomersAttribute(con);
        handleCustomersValue = new HandleCustomersValue(con);
        handleStore = new HandleStore(con);
        handleProduct = new HandleProduct(con);
        handleOrder = new HandleOrder(con);
        handleOrderPart = new HandleOrderPart(con);
        handleReminder = new HandleReminder(con);
        handleReason = new HandleReason(con);
    }

    public static DatabaseData getInstance() { return data; }

    private void createConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
            System.out.println("Database connection success.");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
