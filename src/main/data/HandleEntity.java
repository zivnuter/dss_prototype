package main.data;

import java.sql.*;

public class HandleEntity {
    protected ResultSet executeQueryWithText(Connection con, String query) throws SQLException {
        Statement s = con.createStatement();
        return s.executeQuery(query);
    }

    protected int executeUpdateWithGetId(Connection con, String query) throws SQLException {
        PreparedStatement statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        int affectedRows = statement.executeUpdate();
        if (affectedRows == 0) { throw new SQLException("Creating failed, no rows affected."); }
        try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                return (generatedKeys.getInt(1));
            }
            else {
                throw new SQLException("Creating user failed, no ID obtained.");
            }
        }
    }

    protected void executeUpdate(Connection con, String query) throws SQLException {
        PreparedStatement statement = con.prepareStatement(query);
        int affectedRows = statement.executeUpdate();
        if (affectedRows == 0) { throw new SQLException("Creating failed, no rows affected."); }
    }

    protected void executeUpdateWithoutAffectedCheck(Connection con, String query) throws SQLException {
        PreparedStatement statement = con.prepareStatement(query);
        statement.executeUpdate();
    }
}
