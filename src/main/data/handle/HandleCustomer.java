package main.data.handle;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.data.HandleEntity;
import main.model.Customer;
import main.model.CustomersAttribute;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import static java.lang.String.format;
import static main.constants.GeneralConstants.STRING_TRUE;
import static main.constants.SQLCommands.*;
import static main.constants.TableConstants.*;

public class HandleCustomer extends HandleEntity {
    private boolean shouldBeComma = true;
    private Connection con;
    private HandleCustomersValue handleCustomersValue;
    private List<CustomersAttribute> attributes;

    public HandleCustomer(Connection con) {
        this.con = con;
        attributes = (new HandleCustomersAttribute(con)).getAttributes();
        handleCustomersValue = new HandleCustomersValue(con);
    }

    /**
     * Method which inserts customer to a databse.
     * @param customer
     */
    public void insertCustomer(Customer customer) throws SQLException {
        int id = executeUpdateWithGetId(con, getInsertCustomerQuery(customer));
        customer.setId(id);

        for (CustomersAttribute attr : attributes) {
            String insertValuesQuery = (new HandleCustomersValue(con)).getInsertAttributeValueQuery(customer, attr);
            if(insertValuesQuery != null) {
                executeUpdate(con, insertValuesQuery);
            }
        }
    }

    private String getInsertCustomerQuery(Customer customer) {
        String queryAttributes = "";
        String queryValues = "";

        queryAttributes += TABLE_CUSTOMER_FIRST_NAME + "," + TABLE_CUSTOMER_SECOND_NAME;
        queryValues += "'" + customer.getFirstName() + "','" + customer.getSecondName() + "'";

        if(!customer.getPhone().equals("")) {
            queryAttributes += "," + TABLE_CUSTOMER_PHONE;
            queryValues += ",'" + customer.getPhone() + "'";
        }

        if(!customer.getNotes().equals("")) {
            queryAttributes += "," + TABLE_CUSTOMER_NOTES;
            queryValues += ",'" + customer.getNotes() + "'";
        }
        return format(INSERT, TABLE_CUSTOMER, queryAttributes, queryValues);
    }

    /**
     * This method returns ObservableList of all customers.
     */
    public ObservableList<Customer> getAllCustomers() throws SQLException {
        return getAllCustomers(executeQueryWithText(con, format(SELECT+FROM, "*", TABLE_CUSTOMER)));
    }

    private ObservableList<Customer> getAllCustomers(ResultSet rs) throws SQLException {
        ObservableList<Customer> data = FXCollections.observableArrayList();
        while(rs.next()) {
            int id = rs.getInt(TABLE_CUSTOMER_PK);
            String firstName = rs.getString(TABLE_CUSTOMER_FIRST_NAME);
            String secondName = rs.getString(TABLE_CUSTOMER_SECOND_NAME);
            String phone = rs.getString(TABLE_CUSTOMER_PHONE);
            if(phone == null) { phone = ""; }
            String notes = rs.getString(TABLE_CUSTOMER_NOTES);
            if(notes == null) { notes = ""; }
            HashMap<String, SimpleStringProperty> values = handleCustomersValue.getAllValues(
                executeQueryWithText(con, handleCustomersValue.getAllValuesQuery(id)), attributes
            );
            data.add(new Customer(id, firstName, secondName, phone, notes, values));
        }
        return data;
    }

    /**
     * This methods returns list of all customers, but attribute values hashmap is null.
     */
    public ObservableList<Customer> getAllCustomersWithNullValues() throws SQLException {
        return getAllCustomersWithNullValues(executeQueryWithText(con, format(SELECT+FROM, "*", TABLE_CUSTOMER)));
    }

    private ObservableList<Customer> getAllCustomersWithNullValues(ResultSet rs) throws SQLException {
        ObservableList<Customer> data = FXCollections.observableArrayList();
        while(rs.next()) {
            int id = rs.getInt(TABLE_CUSTOMER_PK);
            String firstName = rs.getString(TABLE_CUSTOMER_FIRST_NAME);
            String secondName = rs.getString(TABLE_CUSTOMER_SECOND_NAME);
            String phone = rs.getString(TABLE_CUSTOMER_PHONE);
            if(phone == null) { phone = ""; }
            String notes = rs.getString(TABLE_CUSTOMER_NOTES);
            if(notes == null) { notes = ""; }

            data.add(new Customer(id, firstName, secondName, phone, notes, null));
        }
        return data;
    }

    /**
     * This method returns customer according to some id.
     * @param id
     */
    public Customer getCustomerById(int id) throws SQLException {
        ResultSet rs = executeQueryWithText(con,
                format(SELECT+FROM+WHERE,
                        "*", TABLE_CUSTOMER, TABLE_CUSTOMER_PK, id
                ));
        if(rs.next()) {
            String firstName = rs.getString(TABLE_CUSTOMER_FIRST_NAME);
            String secondName = rs.getString(TABLE_CUSTOMER_SECOND_NAME);
            String phone = rs.getString(TABLE_CUSTOMER_PHONE);
            if(phone == null) { phone = ""; }
            String notes = rs.getString(TABLE_CUSTOMER_NOTES);
            if(notes == null) { notes = ""; }
            return new Customer(id, firstName, secondName, phone, notes, null);
        }
        return null;
    }

    /**
     * This method delete customer with certain id. It also deletes all connected reminders and orders.
     * @param customersId
     */
    public void deleteCustomer(int customersId) throws SQLException {
        executeUpdateWithoutAffectedCheck(con, handleCustomersValue.getDeleteValuesQuery(customersId));
        (new HandleReminder(con)).deleteReminderByCustomer(customersId);
        HandleOrder handleOrder = new HandleOrder(con);
        for (Integer id : handleOrder.getOrderIdForCustomer(customersId)) {
            (new HandleOrderPart(con)).deleteOrderParts(id);
            handleOrder.deleteOrder(id);
        }
        executeUpdate(con, getDeleteCustomerQuery(customersId));
    }

    private String getDeleteCustomerQuery(int id) {
        return format(DELETE + WHERE, TABLE_CUSTOMER, TABLE_CUSTOMER_PK, id);
    }

    /**
     * This method update customer according to the changes made.
     * @param customer
     * @param changes
     */
    public void updateCustomer(Customer customer, Customer changes) throws SQLException {
        String customerQuery = getUpdateCustomerQuery(customer, changes);
        if(customerQuery != null) { executeUpdate(con, customerQuery); }
        for (CustomersAttribute attr: attributes) {
            String query;
            if(changes.getValue(attr.getName()).equals(STRING_TRUE) &&
                    !handleCustomersValue.valueExist(executeQueryWithText(con,
                            format(SELECT+FROM+WHERE+"AND "+UPDATE_COLUMN,
                                    "*", TABLE_VALUE,
                                    TABLE_VALUE_FK_CUSTOMER, customer.getId(),
                                    TABLE_VALUE_FK_ATTRIBUTE, attr.getId())
                    ))) {
                query = handleCustomersValue.getInsertAttributeValueQuery(customer, attr);
            } else {

                query = handleCustomersValue.getUpdateValueQuery(
                        customer.getValue(attr.getName()), changes.getValue(attr.getName()),
                        customer.getId(), attr.getId()
                );
            }
            if(query != null) { executeUpdate(con, query); }
        }
    }

    private String getUpdateCustomerQuery(Customer customer, Customer changes) {
        String changedValues = "";
        changedValues = addChangedValueString(changedValues, changes.getFirstName(), TABLE_CUSTOMER_FIRST_NAME, customer.getFirstName());
        changedValues = addChangedValueString(changedValues, changes.getSecondName(), TABLE_CUSTOMER_SECOND_NAME, customer.getSecondName());
        changedValues = addChangedValueString(changedValues, changes.getPhone(), TABLE_CUSTOMER_PHONE, customer.getPhone());
        changedValues = addChangedValueString(changedValues, changes.getNotes(), TABLE_CUSTOMER_NOTES, customer.getNotes());
        shouldBeComma = true;
        if(changedValues.equals("")) {return null;}
        return format(UPDATE+changedValues+WHERE, TABLE_CUSTOMER, TABLE_CUSTOMER_PK, customer.getId());
    }

    private String addChangedValueString(String changedValues, String text, String column, String value) {
        if(text.equals(STRING_TRUE)) {
            if(!shouldBeComma) { changedValues += ","; }
            else shouldBeComma = false;
            changedValues +=  format(UPDATE_COLUMN, column, value);
        }
        return changedValues;
    }
}
