package main.data.handle;

import main.data.HandleEntity;
import main.model.CustomersAttribute;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.String.format;
import static main.constants.SQLCommands.FROM;
import static main.constants.SQLCommands.INSERT;
import static main.constants.SQLCommands.SELECT;
import static main.constants.TableConstants.*;
import static main.constants.TableConstants.TABLE_ATTRIBUTE_ORDER;

public class HandleCustomersAttribute extends HandleEntity {
    private List<CustomersAttribute> attributes;
    private Connection con;


    public HandleCustomersAttribute(Connection con) {
        this.con = con;
        getListOfAttributes(false);
    }

    /**
     * This method return list of all attributes from the database.
     */
    public List<CustomersAttribute> getAttributes() { return attributes; }

    private void getListOfAttributes(boolean shouldBeCleared) {
        try {
            if(shouldBeCleared) { attributes.clear(); }
            attributes = getListOfAttributes(
                    executeQueryWithText(con, format(SELECT+FROM, "*", TABLE_ATTRIBUTE))
            );
            Collections.sort(attributes);
        } catch (SQLException e) { e.printStackTrace(); }
    }

    private List<CustomersAttribute> getListOfAttributes(ResultSet rs) throws SQLException {
        List<CustomersAttribute> attributes = new ArrayList<>();
        while(rs.next()) {
            int id = rs.getInt(TABLE_ATTRIBUTE_PK);
            String name = rs.getString(TABLE_ATTRIBUTE_NAME);
            String type = rs.getString(TABLE_ATTRIBUTE_TYPE);
            boolean isMandatory = rs.getString(TABLE_ATTRIBUTE_MANDATORY).equals("true");
            int order = rs.getInt(TABLE_ATTRIBUTE_ORDER);
            CustomersAttribute attr = new CustomersAttribute(id, name, type, isMandatory, order);
            attributes.add(attr);
        }
        return attributes;
    }

    /**
     * This method creates attributes according to list of their names.
     * @param attr
     */
    public void createAttributes(String[] attr) throws SQLException {
        int cnt = 0;
        for (String name : attr) {
            cnt++;
            executeUpdate(con, getCreateAttrQuery(name, cnt));
        }
    }

    private String getCreateAttrQuery(String name, int order) {
        String attributes = TABLE_ATTRIBUTE_NAME+","+TABLE_ATTRIBUTE_TYPE+","
                +TABLE_ATTRIBUTE_MANDATORY+","+TABLE_ATTRIBUTE_ORDER;
        String values = "'"+name+"','textfield','";
        if(name.equals("Telefon")) { values += "true'"; }
        else { values += "false'"; }
        values += ",'" + order + "'";
        return format(INSERT, TABLE_ATTRIBUTE, attributes, values);
    }
}
