package main.data.handle;

import javafx.beans.property.SimpleStringProperty;
import main.model.Customer;
import main.model.CustomersAttribute;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import static java.lang.String.format;
import static main.constants.GeneralConstants.STRING_TRUE;
import static main.constants.SQLCommands.*;
import static main.constants.TableConstants.*;

public class HandleCustomersValue {
    private Connection con;

    public HandleCustomersValue(Connection con) { this.con = con;}

    /**
     * This method returns a query for value insert.
     * @param customer
     * @param attribute
     */
    public String getInsertAttributeValueQuery(Customer customer, CustomersAttribute attribute) {
        String queryAttributes = TABLE_VALUE_FK_CUSTOMER + ","
                + TABLE_VALUE_FK_ATTRIBUTE + ","
                + TABLE_VALUE_VALUE + ","
                + TABLE_VALUE_ORDER;
        String queryValues = "'" + customer.getId() + "','" + attribute.getId() + "'";

        String value = customer.getValue(attribute.getName());
        if(value == null || value.equals("")) { return null; }
        queryValues += ",'" + value + "','1'";
        return format(INSERT, TABLE_VALUE, queryAttributes, queryValues);
    }

    /**
     * This method returns delete value query.
     * @param id
     */
    public String getDeleteValuesQuery(int id) {
        return format(DELETE + WHERE, TABLE_VALUE, TABLE_VALUE_FK_CUSTOMER, id);
    }

    /**
     * This method returns query for get all values.
     * @param id
     */
    public String getAllValuesQuery(int id) {
        return format(
                SELECT + FROM + LEFT_JOIN + WHERE, // == QUERY
                TABLE_ATTRIBUTE_NAME + "," + TABLE_VALUE_VALUE, // == FOR SELECT
                TABLE_VALUE, // == FOR FROM
                TABLE_ATTRIBUTE, // == FOR JOIN
                TABLE_ATTRIBUTE, TABLE_ATTRIBUTE_PK, TABLE_VALUE, TABLE_VALUE_FK_ATTRIBUTE, // == FOR ON
                TABLE_VALUE_FK_CUSTOMER, id // == FOR WHERE
        );
    }

    /**
     * This method returns hashmap of mapped attributes to values. It gets it from ResultSet.
     * @param rs
     * @param attributes
     */

    public HashMap<String, SimpleStringProperty> getAllValues(ResultSet rs, List<CustomersAttribute> attributes) throws SQLException {
        HashMap<String, SimpleStringProperty> values = new HashMap<>();
        for (CustomersAttribute element : attributes) { values.put(element.getName(), new SimpleStringProperty("")); } // initialize all attributes to ""
        while(rs.next()) {
            String attrName = rs.getString(TABLE_ATTRIBUTE_NAME);
            SimpleStringProperty attrValue = new SimpleStringProperty(rs.getString(TABLE_VALUE_VALUE));
            if(attrValue.get() == null) {
                attrValue = new SimpleStringProperty("");
            }
            values.put(attrName, attrValue);
        }
        return values;
    }

    /**
     * This method returns value update query. If there was not change, it returns null.
     * @param value
     * @param changes
     * @param custId
     * @param attrId
     */
    public String getUpdateValueQuery(String value, String changes, int custId, int attrId) {
        if(changes.equals(STRING_TRUE)) {
            return format(UPDATE+UPDATE_COLUMN+WHERE+" AND "+UPDATE_COLUMN,
                    TABLE_VALUE, TABLE_VALUE_VALUE, value,
                    TABLE_VALUE_FK_CUSTOMER, custId,
                    TABLE_VALUE_FK_ATTRIBUTE, attrId
                    );
        }
        else { return null; }
    }

    /**
     * This method returns whether there is some value in ResultSet.
     * @param rs
     */
    public boolean valueExist(ResultSet rs) throws SQLException {
        return rs.isBeforeFirst();
    }
}
