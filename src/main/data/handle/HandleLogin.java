package main.data.handle;

import main.data.HandleEntity;
import main.model.Login;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.String.format;
import static main.constants.SQLCommands.*;
import static main.constants.TableConstants.*;

public class HandleLogin extends HandleEntity {
    private Connection con;

    public HandleLogin(Connection con) { this.con = con; }

    /**
     * This method returns login data.
     */
    public Login getLoginData() throws SQLException {
        return getLoginData(
                executeQueryWithText(con, format(SELECT+FROM, "*", TABLE_LOGIN))
        );
    }

    private Login getLoginData(ResultSet rs) throws SQLException {
        Login login = new Login();
        while(rs.next()) {
            login.setId(rs.getInt(TABLE_LOGIN_ID));
            login.setEmail(rs.getString(TABLE_LOGIN_EMAIL));
            login.setPassword(rs.getString(TABLE_LOGIN_PASSWORD));
        }
        return login;
    }

    /**
     * This method creates login data.
     * @param email
     * @param pass
     */
    public void createLogin(String email, String pass) throws SQLException {
        executeUpdate(con, getCreateLoginQuery(email, pass));
    }

    private String getCreateLoginQuery(String email, String password) {
        return format(INSERT, TABLE_LOGIN,
                TABLE_LOGIN_EMAIL + "," + TABLE_LOGIN_PASSWORD,
                "'" + email + "','" + password + "'");
    }
}
