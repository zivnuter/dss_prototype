package main.data.handle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.data.HandleEntity;
import main.model.Customer;
import main.model.Order;
import main.model.OrderPart;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.lang.String.format;
import static main.constants.SQLCommands.*;
import static main.constants.TableConstants.*;

public class HandleOrder extends HandleEntity {
    private Connection con;

    public HandleOrder(Connection con) { this.con = con; }

    /**
     * This method returns all orders from database in a list.
     */
    public ObservableList<Order> getOrders() throws SQLException {
        return getOrders(executeQueryWithText(con, format(SELECT+FROM, "*", TABLE_ORDER)));
    }

    private ObservableList<Order> getOrders(ResultSet rs) throws SQLException {
        ObservableList<Order> data = FXCollections.observableArrayList();
        while(rs.next()) {
            int id = rs.getInt(TABLE_ORDER_PK);
            Date date = rs.getDate(TABLE_ORDER_DATE);
            String status = rs.getString(TABLE_ORDER_STATUS);
            double price = rs.getDouble(TABLE_ORDER_PRICE);
            String notes = rs.getString(TABLE_ORDER_NOTES);
            int customerId = rs.getInt(TABLE_ORDER_FK_CUSTOMER);
            List<OrderPart> parts = (new HandleOrderPart(con)).getAllOrderParts(
                    executeQueryWithText(con, format(SELECT + FROM + WHERE, //==QUERY
                            "*", TABLE_ORDERPART, TABLE_ORDER_PK, id
                    )) //TODO FUTURE - left join na produkt kvuli aktualnim informacim - endurance napriklad
            );
            Customer c = (new HandleCustomer(con)).getCustomerById(customerId);
            String customer;
            if(c == null) { customer = ""; }
            else { customer = c.getSecondName() + " " + c.getFirstName(); }
            data.add(new Order(id, date, status, price, parts, notes, customer, customerId));
        }
        return data;
    }

    /**
     * This method returns only orders which need to be delivered.
     */
    public ObservableList<Order>  getOrdersToExecute() throws SQLException {
        return getOrders(executeQueryWithText(con, format(
                SELECT+FROM+WHERE_NOT, "*", TABLE_ORDER,
                TABLE_ORDER_STATUS, "doručeno"
                )));
    }

    /**
     * This method returns only orders for some certain customer.
     * @param id
     */
    public ObservableList<Order> getOrdersForCustomer(int id) throws SQLException {
        return getOrders(executeQueryWithText(con, format(
                SELECT+FROM+WHERE, "*", TABLE_ORDER,
                TABLE_ORDER_FK_CUSTOMER, id
        )));
    }

    /**
     * This method returns list of order ids, which belongs to given customer.
     * @return
     */
    public List<Integer> getOrderIdForCustomer(int id) throws SQLException {
        List<Integer> ids = new ArrayList<>();
        ResultSet rs = executeQueryWithText(con, format(
                SELECT+FROM+WHERE, "*", TABLE_ORDER,
                TABLE_ORDER_FK_CUSTOMER, id
        ));
        while(rs.next()) {
            ids.add(rs.getInt(TABLE_ORDER_PK));
        }
        return ids;
    }

    /**
     * This method insert given order into database.
     * @param order
     */
    public int insertOrder(Order order) throws SQLException {
        return executeUpdateWithGetId(con, getInsertOrderQuery(order));
    }

    private String getInsertOrderQuery(Order order) {
        String attributes = TABLE_ORDER_FK_CUSTOMER + "," + TABLE_ORDER_STATUS + "," + TABLE_ORDER_PRICE;
        String values = "'"+order.getCustomersId() + "','" + order.getStatus() + "','" + order.getPrice() + "'";

        if(order.getDate() != null) {
            attributes += "," + TABLE_ORDER_DATE;
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            values += ",'" + df.format(order.getDate()) + "'";
        }
        if(!order.getNotes().equals("")) {
            attributes += "," + TABLE_ORDER_NOTES;
            values += ",'" + order.getNotes() + "'";
        }

        return format(INSERT, TABLE_ORDER, attributes, values);
    }

    /**
     * Deletes order with given id.
     * @param id
     */
    public void deleteOrder(int id) throws SQLException {
        executeUpdate(con, format(
                DELETE+WHERE, TABLE_ORDER,
                TABLE_ORDER_PK, id
        ));
    }
}
