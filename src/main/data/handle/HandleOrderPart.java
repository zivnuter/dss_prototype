package main.data.handle;

import main.data.HandleEntity;
import main.model.OrderPart;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static main.constants.SQLCommands.DELETE;
import static main.constants.SQLCommands.INSERT;
import static main.constants.SQLCommands.WHERE;
import static main.constants.TableConstants.*;

public class HandleOrderPart extends HandleEntity {
    private Connection con;

    public HandleOrderPart(Connection con) { this.con = con; }

    /**
     * This method returns all order parts for certain order from database.
     * @param rs
     */
    public List<OrderPart> getAllOrderParts(ResultSet rs) throws SQLException {
        List<OrderPart> data = new ArrayList<>();
        while(rs.next()) {
            int id = rs.getInt(TABLE_ORDERPART_PK);
            int count = rs.getInt(TABLE_ORDERPART_COUNT);
            String name = rs.getString(TABLE_ORDERPART_NAME);
            double price = rs.getDouble(TABLE_ORDERPART_PRICE);
//            double endurance = rs.getDouble(TABLE_ORDERPART_ENDURANCE);
            int fkOrder = rs.getInt(TABLE_ORDERPART_FK_ORDER);
            int fkProduct = rs.getInt(TABLE_ORDERPART_FK_PRODUCT);

            data.add(new OrderPart(id, count, name, price, /*endurance,*/ fkOrder, fkProduct));
        }
        return data;
    }

    /**
     * This method insert list of orders parts into the database.
     * @param orderParts
     */
    public void insertOrderParts(List<OrderPart> orderParts) throws SQLException {
        for (OrderPart part : orderParts) {
            executeUpdate(con, getInsertOrderPartQuery(part));
        }
    }

    private String getInsertOrderPartQuery(OrderPart part) {
        String attributes = TABLE_ORDERPART_COUNT + "," + TABLE_ORDERPART_NAME + "," + TABLE_ORDERPART_PRICE
                + "," + TABLE_ORDERPART_FK_ORDER + "," + TABLE_ORDERPART_FK_PRODUCT;
        String values = "'" + part.getCount() + "','" + part.getProductName() + "','"
                + part.getPrice() + "','" + part.getFkOrder() + "','" +  part.getFkProduct() + "'";

        return format(INSERT, TABLE_ORDERPART, attributes, values);
    }

    /**
     * Delete all orderParts for order with orderId.
     * @param orderId
     */
    public void deleteOrderParts(int orderId) throws SQLException {
        executeUpdate(con, format(
                DELETE+WHERE, TABLE_ORDERPART,
                TABLE_ORDERPART_FK_ORDER, orderId
        ));
    }
}
