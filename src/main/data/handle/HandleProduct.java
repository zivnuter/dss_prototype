package main.data.handle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.data.HandleEntity;
import main.model.Product;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.String.format;
import static main.constants.GeneralConstants.STRING_TRUE;
import static main.constants.SQLCommands.*;
import static main.constants.TableConstants.*;

public class HandleProduct extends HandleEntity {
    private boolean shouldBeComma = true;
    private Connection con;

    public HandleProduct(Connection con) { this.con = con;}

    /**
     * This method returns list of all Products from the database.
     */
    public ObservableList<Product> getAllProducts() throws SQLException {
        return getAllProducts(executeQueryWithText(con, format(SELECT+FROM, "*", TABLE_PRODUCT)));
    }

    private ObservableList<Product> getAllProducts(ResultSet resultSet) throws SQLException {
        ObservableList<Product> data = FXCollections.observableArrayList();
        while(resultSet.next()) {
            int id = resultSet.getInt(TABLE_PRODUCT_PK);
            String name = resultSet.getString(TABLE_PRODUCT_NAME);
            double price = resultSet.getDouble(TABLE_PRODUCT_PRICE);
            int count = resultSet.getInt(TABLE_PRODUCT_COUNT);
            double endurance = resultSet.getDouble(TABLE_PRODUCT_ENDURANCE);
            String notes = resultSet.getString(TABLE_PRODUCT_NOTES);
            if(notes == null) { notes = ""; }
            data.add(new Product(id, name, price, count, endurance, notes));
        }
        return data;
    }

    /**
     * This method returns Product object according to id.
     * @param productId
     */
    public Product getProductsById(int productId) throws SQLException {
        ResultSet resultSet =  executeQueryWithText(con, format(SELECT+FROM+WHERE, "*", TABLE_PRODUCT,
                TABLE_PRODUCT_PK, productId));
        if(resultSet.next()) {
            int id = resultSet.getInt(TABLE_PRODUCT_PK);
            String name = resultSet.getString(TABLE_PRODUCT_NAME);
            int price = resultSet.getInt(TABLE_PRODUCT_PRICE);
            int count = resultSet.getInt(TABLE_PRODUCT_COUNT);
            int endurance = resultSet.getInt(TABLE_PRODUCT_ENDURANCE);
            String notes = resultSet.getString(TABLE_PRODUCT_NOTES);
            if(notes == null) { notes = ""; }
            return  (new Product(id, name, price, count, endurance, notes));
        }
        return null;
    }

    /**
     * This method deletes product according to some id.
     * @param id
     */
    public void deleteProduct(int id) throws SQLException {
        executeUpdate(con, getDeleteProductQuery(id));
    }

    private String getDeleteProductQuery(int id) {
        return format(DELETE+WHERE, TABLE_PRODUCT, TABLE_PRODUCT_PK, id);
    }

    /**
     * This method inserts certain product to a database.
     * @param product
     */
    public void insertProduct(Product product) throws SQLException {
        executeUpdate(con, getCreateProductQuery(product));
    }

    private String getCreateProductQuery(Product product) {
        String attributes = TABLE_PRODUCT_NAME + "," + TABLE_PRODUCT_PRICE + "," + TABLE_PRODUCT_COUNT;
        String values = "'" + product.getName() + "','" + product.getPrice() + "','" + product.getCount() + "'";

        if(product.getEndurance() != Integer.MIN_VALUE) {
            attributes += "," + TABLE_PRODUCT_ENDURANCE;
            values += ",'" + product.getEndurance() + "'";
        }
        if(!product.getNotes().equals("")) {
            attributes += "," + TABLE_PRODUCT_NOTES;
            values += ",'" + product.getNotes() + "'";
        }
        return format(INSERT, TABLE_PRODUCT, attributes, values);
    }

    /**
     * This method updates product according to changes made.
     * @param product
     * @param changes
     */
    public void updateProduct(Product product, Product changes) throws SQLException {
        executeUpdate(con, getUpdateProductQuery(product, changes));
    }

    private String getUpdateProductQuery(Product product, Product changes) {
        String changedValues = "";
        changedValues = addChangedValueString(changedValues, changes.getName(), TABLE_PRODUCT_NAME, product.getName());
        changedValues = addChangedValueDouble(changedValues, changes.getPrice(), TABLE_PRODUCT_PRICE, product.getPrice());
        changedValues = addChangedValueIneteger(changedValues, changes.getCount(), TABLE_PRODUCT_COUNT, product.getCount());
        changedValues = addChangedValueDouble(changedValues, changes.getEndurance(), TABLE_PRODUCT_ENDURANCE, product.getEndurance());
        changedValues = addChangedValueString(changedValues, changes.getNotes(), TABLE_PRODUCT_NOTES, product.getNotes());
        shouldBeComma = true;
        return format(UPDATE + changedValues + WHERE, TABLE_PRODUCT, TABLE_PRODUCT_PK, product.getId());
    }

    private String addChangedValueString(String changedValues, String text, String column, String value) {
        if(text.equals(STRING_TRUE)) {
            if(!shouldBeComma) { changedValues += ","; }
            else shouldBeComma = false;
            changedValues +=  format(UPDATE_COLUMN, column, value);
        }
        return changedValues;
    }

    private String addChangedValueIneteger(String changedValues, int num, String column, int value) {
        if(num == Integer.MAX_VALUE) {
            if (!shouldBeComma) { changedValues += ","; }
            else { shouldBeComma = false; }
            changedValues += format(UPDATE_COLUMN, TABLE_PRODUCT_ENDURANCE, value);
        }
        return changedValues;
    }

    private String addChangedValueDouble(String changedValues, double num, String column, double value) {
        if(num == Integer.MAX_VALUE) {
            if (!shouldBeComma) { changedValues += ","; }
            else { shouldBeComma = false; }
            changedValues += format(UPDATE_COLUMN, TABLE_PRODUCT_ENDURANCE, value);
        }
        return changedValues;
    }
}
