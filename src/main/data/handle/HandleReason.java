package main.data.handle;

import main.data.HandleEntity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.String.format;
import static main.constants.SQLCommands.*;
import static main.constants.TableConstants.TABLE_REASON;
import static main.constants.TableConstants.TABLE_REASON_NAME;
import static main.constants.TableConstants.TABLE_REASON_PK;

public class HandleReason extends HandleEntity {
    private Connection con;

    public HandleReason(Connection con) { this.con = con;}

    /**
     * This method returns id of a reason according to given name.
     * @param name
     * @return
     * @throws SQLException
     */
    public int getIdByReasonName(String name) throws SQLException {
        ResultSet rs = executeQueryWithText(con, format(
                SELECT+FROM+WHERE, TABLE_REASON_PK, TABLE_REASON,
                TABLE_REASON_NAME, name
        ));

        if(rs.next()) {
            return rs.getInt(TABLE_REASON_PK);
        }
        return 0;
    }

    /**
     * This methods returns reasons by id.
     * @param id
     * @return
     * @throws SQLException
     */
    public String getReasonById(int id) throws SQLException {
        ResultSet rs = executeQueryWithText(con, format(
                SELECT+FROM+WHERE, "*", TABLE_REASON,
                TABLE_REASON_PK, id
        ));

        if(rs.next()) {
            return rs.getString(TABLE_REASON_NAME);
        }
        return "";
    }

    /**
     * This method insert given reasons to the database.
     * @param reasons
     */
    public void insertReasons(String[] reasons) throws SQLException {
        for (String text : reasons) {
            executeUpdate(con, format(INSERT, TABLE_REASON, TABLE_REASON_NAME,
                    "'"+text+"'"));
        }
    }

    /**
     * This method returns all reasons from database as an array of strings.
     * @return
     */
    public String[] getAllReasons() throws SQLException {
        return getAllReasons(executeQueryWithText(con, format(
                SELECT+FROM, TABLE_REASON_NAME, TABLE_REASON
        )));
    }

    private String[] getAllReasons(ResultSet rs) throws SQLException {
        int num = 0;
        if (rs.last()) {
            num = rs.getRow();
            rs.beforeFirst();
        }
        String[] reasons = new String[num];
        int cnt = 0;
        while(rs.next()) {
            reasons[cnt] = rs.getString(TABLE_REASON_NAME);
            cnt++;
        }
        return reasons;
    }
}
