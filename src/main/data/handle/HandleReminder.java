package main.data.handle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.data.HandleEntity;
import main.model.Reminder;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.String.format;
import static main.constants.SQLCommands.*;
import static main.constants.TableConstants.*;

public class HandleReminder extends HandleEntity {
    private Connection con;

    public HandleReminder(Connection con) { this.con = con;}

    /**
     * This method return all reminders connected to customer with given id.
     * @param id
     */
    public ObservableList<Reminder> getAllReminders(int id) throws SQLException {
        return getAllReminders(executeQueryWithText(con, format(
                SELECT+FROM+WHERE, "*", TABLE_REMINDERS,
                TABLE_REMINDERS_FK_CUSTOMER, id
        )));
    }

    private ObservableList<Reminder> getAllReminders(ResultSet resultSet) throws SQLException {
        ObservableList<Reminder> data = FXCollections.observableArrayList();

        while(resultSet.next()) {
            int id = resultSet.getInt(TABLE_REMINDERS_PK);
            int customerId = resultSet.getInt(TABLE_REMINDERS_FK_CUSTOMER);
            Date date = resultSet.getDate(TABLE_REMINDERS_DATE);
            int reasonId = resultSet.getInt(TABLE_REMINDERS_FK_REASON);
            String reason = (new HandleReason(con)).getReasonById(reasonId);
            String notes = resultSet.getString(TABLE_REMINDERS_NOTES);
            if(notes == null) { notes = ""; }
            data.add(new Reminder(id, customerId, reason, date, notes));
        }
        return data;
    }


    /**
     * This method insert given reminder to the database.
     * @param reminder
     * @throws SQLException
     */
    public void insertReminder(Reminder reminder) throws SQLException {
        executeUpdate(con,getInsertReminderQuery(reminder));
    }

    private String getInsertReminderQuery(Reminder reminder) throws SQLException {
        int reasonId = (new HandleReason(con)).getIdByReasonName(reminder.getReason());
        String attributes = TABLE_REMINDERS_FK_CUSTOMER + "," + TABLE_REMINDERS_FK_REASON;
        String values = "'"+reminder.getFkCustomer() + "','" + reasonId+"'";
        attributes += "," + TABLE_REMINDERS_DATE;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        values += ",'" + df.format(reminder.getDate()) + "'";

        if(!reminder.getNotes().equals("")) {
            attributes += "," + TABLE_ORDER_NOTES;
            values += ",'" + reminder.getNotes() + "'";
        }

        return format(INSERT, TABLE_REMINDERS, attributes, values);
    }

    /**
     * This method deletes reminder with certain id.
     * @param id
     * @throws SQLException
     */
    public void deleteReminder(int id) throws SQLException{
        executeUpdate(con, format(
                DELETE+WHERE, TABLE_REMINDERS, TABLE_REMINDERS_PK, id
        ));
    }

    /**
     * This method deletes all reminders connected to certain customer.
     * @param id
     * @throws SQLException
     */
    public void deleteReminderByCustomer(int id) throws SQLException{
        executeUpdateWithoutAffectedCheck(con, format(
                DELETE+WHERE, TABLE_REMINDERS, TABLE_REMINDERS_FK_CUSTOMER, id
        ));
    }

    public void updateReminder(Reminder reminder) throws SQLException {
        executeUpdate(con, getUpdateReminderQuery(reminder));
    }

    private String getUpdateReminderQuery(Reminder reminder) throws SQLException {
        String changedValues;
        changedValues = format(UPDATE_COLUMN, TABLE_REMINDERS_FK_CUSTOMER, reminder.getFkCustomer());
        int reasonId = (new HandleReason(con)).getIdByReasonName(reminder.getReason());
        changedValues += "," + format(UPDATE_COLUMN, TABLE_REMINDERS_FK_REASON, reasonId);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(reminder.getDate());
        changedValues += "," + format(UPDATE_COLUMN, TABLE_REMINDERS_DATE, date);
        changedValues += "," + format(UPDATE_COLUMN, TABLE_REMINDERS_NOTES, reminder.getNotes());
        return format(
                UPDATE+changedValues+WHERE, TABLE_REMINDERS, TABLE_REMINDERS_PK, reminder.getId()
        );
    }
}
