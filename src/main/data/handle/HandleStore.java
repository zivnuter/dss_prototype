package main.data.handle;

import main.data.HandleEntity;
import main.model.Store;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.String.format;
import static main.constants.SQLCommands.*;
import static main.constants.TableConstants.*;

public class HandleStore extends HandleEntity {
    private Connection con;

    public HandleStore(Connection con) { this.con = con;}

    /**
     * This method insert a single store into the database.
     * @param name
     * @param isDefault
     */
    public void createStore(String name, int isDefault) throws SQLException {
        executeUpdate(con, getCreateStoreQuery(name, isDefault));
    }

    private String getCreateStoreQuery(String name, int isDefault) {
        return format(INSERT, TABLE_STORE,
                TABLE_STORE_NAME + "," + TABLE_STORE_DEFAULT,
                "'" + name + "','" + isDefault + "'");
    }

    /**
     * This method returns all stores from the database.
     */
    public Store getAllStores() throws SQLException {
        return getStore(executeQueryWithText(con, format(SELECT+FROM, "*", TABLE_STORE)));
    }

    private Store getStore(ResultSet rs) throws SQLException {
        int id = 0, isDefault = 0, cnt = 0;
        String name = "";
        while(rs.next()) {
            id = rs.getInt(TABLE_STORE_PK);
            isDefault = rs.getInt(TABLE_STORE_DEFAULT);
            name = rs.getString(TABLE_STORE_NAME);
            if(cnt >= 1) { break; }
            cnt++;
        }
        if(cnt == 0) { return null; }
        return new Store(id, name, isDefault);
    }
}
