package main.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

public class Customer extends RecursiveTreeObject<Customer> {
    private HashMap<String, SimpleStringProperty> valuesOfAttributes;
    private SimpleIntegerProperty id;
    private SimpleStringProperty firstName;
    private SimpleStringProperty secondName;
    private String phone;
    private SimpleStringProperty notes;

    public Customer(int id, String firstName, String secondName, String phone,  String notes, HashMap<String, SimpleStringProperty> attributes) {
        this.id = new SimpleIntegerProperty(id);
        this.firstName = new SimpleStringProperty(firstName);
        this.secondName = new SimpleStringProperty(secondName);
        this.phone = phone;
        this.notes = new SimpleStringProperty(notes);
        this.valuesOfAttributes = attributes;
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) { this.id = new SimpleIntegerProperty(id); }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) { this.firstName.set(firstName); }

    public String getSecondName() {
        return secondName.get();
    }

    public void setSecondName(String secondName) { this.secondName.set(secondName); }

    public String getPhone() { return phone; }

    public void setPhone(String phone) { this.phone = phone; }

    public String getNotes() { return notes.get(); }

    public void setNotes(String notes) { this.notes.set(notes); }

    public HashMap<String, SimpleStringProperty> getValuesOfAttributes() { return valuesOfAttributes; }

    public String getValue(String name) {
        if(valuesOfAttributes.get(name) == null) { return null; }
        else { return valuesOfAttributes.get(name).get(); }
    }

    public void setValue(String name, String value) {
        valuesOfAttributes.put(name, new SimpleStringProperty(value));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;

        return hashMapValuesEquals(customer.valuesOfAttributes) &&
                Objects.equals(id.get(), customer.id.get()) &&
                Objects.equals(firstName.get(), customer.firstName.get()) &&
                Objects.equals(secondName.get(), customer.secondName.get()) &&
                Objects.equals(phone, customer.phone) &&
                Objects.equals(notes.get(), customer.notes.get());
    }

    private boolean hashMapValuesEquals(HashMap<String, SimpleStringProperty> map) {
        if(valuesOfAttributes == null && map == null) { return true; }
//        if((valuesOfAttributes == null && map != null) || (valuesOfAttributes != null && map == null)) { return false; }
        for (String key : valuesOfAttributes.keySet()) {
            if(!valuesOfAttributes.get(key).get().equals(map.get(key).get())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(valuesOfAttributes, id, firstName, secondName, phone, notes);
    }
}
