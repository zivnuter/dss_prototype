package main.model;

public class CustomersAttribute implements Comparable<CustomersAttribute> {
    private int id;
    private String name;
    private String type;
    private Boolean isMandatory;
    private int order;

    public CustomersAttribute(int id, String name, String type, Boolean isMandatory, int order) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.isMandatory = isMandatory;
        this.order = order;
    }

    public int getId() { return id; }

    public String getName() { return name; }

    public String getType() { return type; }

    public Boolean getMandatory() { return isMandatory; }

    public int getOrder() { return order; }

    @Override
    public int compareTo(CustomersAttribute attr) {
        int order = ((CustomersAttribute)attr).getOrder();
        return this.order-order;
    }

    @Override
    public String toString() {
        return "CustomersAttribute{" + "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", isMandatory=" + isMandatory +
                ", order='" + order + '\'' +
                '}';
    }
}
