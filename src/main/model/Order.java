package main.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Order extends RecursiveTreeObject<Order> {
    private SimpleIntegerProperty id;
    private Date date;
    private String status;
    private double price;
    private List<OrderPart> items;
    private SimpleStringProperty notes;
    private String customer;
    private int customersId;

    public Order(int id, Date date, String status, double price, List<OrderPart> items, String notes, String customer, int customersId) {
        this.id = new SimpleIntegerProperty(id);
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        this.date = date;
        this.status = status;
        this.price = price;
        this.items = items;
        this.notes = new SimpleStringProperty(notes);
        this.customer = customer;
        this.customersId = customersId;
    }

    public Order(double price) {
        this.price = price;
    }

    public int getId() { return id.get(); }

    public void setId(int id) { this.id.set(id); }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public String getStatus() { return status; }

    public void setStatus(String status) { this.status = status; }

    public double getPrice() { return price; }

    public void setPrice(double price) { this.price = price; }

    public List<OrderPart> getItems() { return items; }

    public void setItems(List<OrderPart> items) { this.items = items; }

    public String getNotes() { return notes.get(); }

    public void setNotes(String notes) { this.notes.set(notes); }

    public String getCustomer() { return customer; }

    public void setCustomer(String customer) { this.customer = customer; }

    public int getCustomersId() { return customersId; }

    public void setCustomersId(int customersId) { this.customersId = customersId; }
}
