package main.model;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class OrderPart {
    private SimpleIntegerProperty id;
    private SimpleIntegerProperty count;
    private SimpleStringProperty productName;
    private SimpleDoubleProperty price;
//    private double endurance;
    private int fkOrder;
    private int fkProduct;

    public OrderPart(int id, int count, String productName, double price, /*double endurance, */int fkOrder, int fkProduct) {
        this.id = new SimpleIntegerProperty(id);
        this.count = new SimpleIntegerProperty(count);
        this.productName = new SimpleStringProperty(productName);
        this.price = new SimpleDoubleProperty(price);
//        this.endurance = endurance;
        this.fkOrder = fkOrder;
        this.fkProduct = fkProduct;
    }

    public OrderPart(int count) {
        this.count = new SimpleIntegerProperty(count);
    }

    public int getId() { return id.get(); }

    public void setId(int id) { this.id.set(id); }

    public int getCount() { return count.get(); }

    public void setCount(int count) { this.count.set(count); }

    public String getProductName() { return productName.get(); }

    public void setProductName(String productName) { this.productName.set(productName); }

    public double getPrice() { return price.get(); }

    public void setPrice(double price) { this.price.set(price); }

//    public double getEndurance() { return endurance; }

//    public void setEndurance(double endurance) { this.endurance = endurance; }

    public int getFkOrder() { return fkOrder; }

    public void setFkOrder(int fkOrder) { this.fkOrder = fkOrder; }

    public int getFkProduct() { return fkProduct; }

    public void setFkProduct(int fkProduct) { this.fkProduct = fkProduct; }
}
