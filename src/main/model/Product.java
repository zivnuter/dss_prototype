package main.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.Objects;

public class Product extends RecursiveTreeObject<Product> {
    private SimpleIntegerProperty id;
    private SimpleStringProperty name;
    private SimpleDoubleProperty price;
    private SimpleIntegerProperty count;
    private SimpleDoubleProperty endurance;
    private SimpleStringProperty notes;

    public Product(int id, String name, double price, int count, double endurance, String notes) {
        this.id = new SimpleIntegerProperty(id);
        this.name = new SimpleStringProperty(name);
        this.price = new SimpleDoubleProperty(price);
        this.count = new SimpleIntegerProperty(count);
        this.endurance = new SimpleDoubleProperty(endurance);
        this.notes = new SimpleStringProperty(notes);
    }

    public int getId() { return id.get(); }

    public void setId(int id) { this.id.set(id); }

    public String getName() { return name.get(); }

    public void setName(String name) { this.name.set(name); }

    public double getPrice() { return price.get(); }

    public void setPrice(double price) { this.price.set(price); }

    public int getCount() { return count.get(); }

    public void setCount(int count) { this.count.set(count); }

    public double getEndurance() { return endurance.get(); }

    public void setEndurance(double endurance) { this.endurance.set(endurance); }

    public String getNotes() { return notes.get(); }

    public void setNotes(String notes) { this.notes.set(notes); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id.get(), product.id.get()) &&
                Objects.equals(name.get(), product.name.get()) &&
                Objects.equals(price.get(), product.price.get()) &&
                Objects.equals(count.get(), product.count.get()) &&
                Objects.equals(endurance.get(), product.endurance.get()) &&
                Objects.equals(notes.get(), product.notes.get());
    }
}
