package main.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import java.util.Date;
import java.util.Objects;

public class Reminder extends RecursiveTreeObject<Reminder> {
    private int id;
    private int fkCustomer;
    private String reason;
    private Date date;
    private String notes;

    public Reminder(int id, int fkCustomer, String reason, Date date, String notes) {
        this.id = id;
        this.fkCustomer = fkCustomer;
        this.reason = reason;
        this.date = date;
        this.notes = notes;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public int getFkCustomer() { return fkCustomer; }

    public void setFkCustomer(int fkCustomer) { this.fkCustomer = fkCustomer; }

    public String getReason() { return reason; }

    public void setReason(String reason) { this.reason = reason; }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public String getNotes() { return notes; }

    public void setNotes(String notes) { this.notes = notes; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reminder reminder = (Reminder) o;
        return id == reminder.id &&
                fkCustomer == reminder.fkCustomer &&
                Objects.equals(reason, reminder.reason) &&
                Objects.equals(date, reminder.date) &&
                Objects.equals(notes, reminder.notes);
    }
}
