package main.model;

public class Store {
    private int id;
    private String name;
    private int isDefault;

    public Store(int id, String name, int isDefault) {
        this.id = id;
        this.name = name;
        this.isDefault = isDefault;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public int getIsDefault() { return isDefault; }

    public void setIsDefault(int isDefault) { this.isDefault = isDefault; }
}
