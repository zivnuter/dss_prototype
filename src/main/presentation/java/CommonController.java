package main.presentation.java;

import com.jfoenix.controls.*;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import main.presentation.java.content.customers.profile.CustomersProfileInfoController;
import main.data.DatabaseData;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.DIALOG_FILL_MANDATORY;
import static main.constants.DialogContants.DIALOG_UNDERSTAND;

/**
 * All controllers inherit from this class.
 * So this class contains methods which are common to all of the controllers.
 */
public class CommonController implements Initializable {
    protected DatabaseData data = DatabaseData.getInstance();
    protected static String pathToFxmlPackage = "../fxml";

    @Override
    public void initialize(URL url, ResourceBundle rb) {}

    protected boolean checkEmail(String email) { return email.matches(".+@.+\\..+"); }

    protected void openNewStackPage(StackPane pane, String path) {
        StackPane newPane = null;
        try {
            newPane = FXMLLoader.load(getClass().getResource(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        pane.getChildren().setAll(newPane);
    }

    protected void openNewAnchorPage(AnchorPane pane, String path, CommonController controller) {
        AnchorPane newPane = null;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
            loader.setController(controller);
            newPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        pane.getChildren().setAll(newPane);
    }

    protected void openUnderstandDialogWithText(StackPane stackPane, String withBody) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setBody(new Text(withBody));
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton(DIALOG_UNDERSTAND);
        button.setOnAction(event -> dialog.close());
        content.setActions(button);
        dialog.show();
    }

    protected void addHboxWith(String text, String type, VBox content, List<HBox> infoLines) {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(5, 0, 5, 0));
        hbox.setAlignment(Pos.TOP_RIGHT);
        Label label = new Label(text);
        label.setPadding(new Insets(0, 10, 0, 10));
        if(type.equals("textarea")) {
            JFXTextArea textArea = new JFXTextArea();
            textArea.setStyle("-fx-background-color: white; -jfx-unfocus-color: transparent; -jfx-focus-color: #4e8397;");
            textArea.setMaxSize(400, 80);
            hbox.getChildren().addAll(label, textArea);
        } else if(type.equals("datepicker")) {
            JFXDatePicker datePicker = new JFXDatePicker();
            datePicker.setStyle("-fx-background-color: white; -jfx-unfocus-color: transparent; -jfx-focus-color: #4e8397;");
            datePicker.setMinWidth(400);
            datePicker.setMaxWidth(400);
            hbox.getChildren().addAll(label, datePicker);
        } else {
            JFXTextField textField = new JFXTextField();
            textField.setStyle("-fx-background-color: white; -jfx-unfocus-color: transparent; -jfx-focus-color: #4e8397");
            textField.setPrefSize(400, 20);
            hbox.getChildren().addAll(label, textField);
        }
        content.getChildren().addAll(hbox);
        infoLines.add(hbox);
    }

    protected void addHboxWith(String text, VBox content, List<HBox> infoLines, String[] comboboxText) {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(5, 0, 5, 0));
        hbox.setAlignment(Pos.TOP_RIGHT);
        Label label = new Label(text);
        label.setPadding(new Insets(0, 10, 0, 10));
        JFXComboBox comboBox = new JFXComboBox();
        comboBox.setMinWidth(400);
        comboBox.setStyle("-fx-background-color: white; -jfx-unfocus-color: transparent; -jfx-focus-color: #4e8397;");
        for (String comboLabel : comboboxText) {
            comboBox.getItems().add(comboLabel);
        }
        hbox.getChildren().addAll(label, comboBox);
        content.getChildren().addAll(hbox);
        infoLines.add(hbox);
    }

    protected boolean checkMandatoryFields(StackPane stackPane, List<HBox> infoLines) {
        boolean error = false;
        for (HBox hbox : infoLines) {
            Node nodeL = hbox.getChildren().get(0);
            Node nodeT = hbox.getChildren().get(1);
            String label = "", textfield = "";

            if(nodeL instanceof Label) { label = ((Label)nodeL).getText(); }
            if(nodeT instanceof JFXTextArea) { textfield = ((JFXTextArea)nodeT).getText(); }
            if(nodeT instanceof JFXTextField) { textfield = ((JFXTextField)nodeT).getText(); }
            if(nodeT instanceof JFXComboBox) {
                if(((JFXComboBox)nodeT).getSelectionModel().getSelectedItem() != null) { textfield = "a"; }
            }
            if(nodeT instanceof JFXDatePicker) {
                if(((JFXDatePicker)nodeT).getValue() != null) { textfield = "a"; }
            }

            if(label.contains("*:") && textfield.equals("")) {
                error = true;
            }
        }
        if(error) {
            openUnderstandDialogWithText(stackPane, DIALOG_FILL_MANDATORY);
            return false;
        }
        return true;
    }
}
