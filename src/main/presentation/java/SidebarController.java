package main.presentation.java;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This controller handles sidebar menu.
 */
public class SidebarController extends CommonController {
    @FXML
    private BorderPane borderpane;
    @FXML
    private Button home;
    @FXML
    private Button customers;
    @FXML
    private Button orders;
    @FXML
    private Button store;
    @FXML
    private Button calendar;
    @FXML
    private Button statistics;

    private Button previousBookmark;

    @Override public void initialize(URL url, ResourceBundle rb) {
        previousBookmark = null;
        changeColor(home);
        loadUI("home");
    }

    /**
     * This method opens customers bookmark.
     * @param mouseEvent
     */
    public void customers(MouseEvent mouseEvent) {
        changeColor(customers);
        loadUI("customers");
    }

    /**
     * This method opens calendar bookmark.
     * @param mouseEvent
     */
    public void calendar(MouseEvent mouseEvent) {
        changeColor(calendar);
        loadUI("calendar");
    }

    /**
     * This method opens orders bookmark.
     * @param mouseEvent
     */
    public void orders(MouseEvent mouseEvent) {
        changeColor(orders);
        loadUI("orders");
    }

    /**
     * This method opens store bookmark.
     * @param mouseEvent
     */
    public void store(MouseEvent mouseEvent) {
        changeColor(store);
        loadUI("store");
    }

    /**
     * This method opens home bookmark.
     * @param mouseEvent
     */
    public void home(MouseEvent mouseEvent) {
        changeColor(home);
        loadUI("home");
    }

    /**
     * This method opens statistics bookmark.
     * @param mouseEvent
     */
    public void statistics(MouseEvent mouseEvent) {
        changeColor(statistics);
        loadUI("statistics");
    }

    private void loadUI(String name) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(pathToFxmlPackage+"/content/"+name+"/"+name+".fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        borderpane.setCenter(root);
    }

    private void changeColor(Button button) {
        if(previousBookmark != null) {
            previousBookmark.setStyle("-fx-background-color: '#263238'");
        }
        button.setStyle("-fx-background-color: '#4e8397'");
        previousBookmark = button;
    }
}
