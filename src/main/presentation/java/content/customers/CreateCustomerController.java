package main.presentation.java.content.customers;

import com.jfoenix.controls.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import main.model.Customer;

import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.*;

/**
 * This controller handles page where customer is created.
 */
public class CreateCustomerController extends CustomersController {
    private List<HBox> infoLines;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private StackPane stackPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) { infoLines = createFields(scrollPane); }

    /**
     * This method handles create button and creates customer.
     * @param mouseEvent
     */
    public void create(MouseEvent mouseEvent) {
        if(!checkMandatoryFields(stackPane, infoLines)) { return; }
        try {
            data.handleCustomer.insertCustomer(createCustomer(infoLines));
            openNewStackPage(stackPane, pathToCustomersPackage + "/customers.fxml");
            openUnderstandDialogWithText(stackPane, CUSTOMER_CREATE_DIALOG_CREATED);
        } catch (SQLException e) {
            openUnderstandDialogWithText(stackPane, DIALOG_ERROR);
            e.printStackTrace();
        }
    }

    /**
     * This method handles back button. It returns user to the overview.
     * @param mouseEvent
     */
    public void back(MouseEvent mouseEvent) {
        openBackYesNoDialogWithText(CUSTOMER_CREATE_DIALOG_TROW);
    }

    private void openBackYesNoDialogWithText(String withBody) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setBody(new Text(withBody));

        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button1 = new JFXButton(DIALOG_YES);
        JFXButton button2 = new JFXButton(DIALOG_NO);
        button1.setOnAction(event -> {
            dialog.close();
            openNewStackPage(stackPane, pathToCustomersPackage+"/customers.fxml");
        });
        button2.setOnAction(event -> dialog.close());
        content.setActions(button1,button2);
        dialog.show();
    }
}
