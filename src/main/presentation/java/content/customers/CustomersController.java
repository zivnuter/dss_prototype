package main.presentation.java.content.customers;

import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import main.presentation.java.CommonController;
import main.model.Customer;
import main.model.CustomersAttribute;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

/**
 * This controller handles customer overview.
 */
public class CustomersController extends CommonController {
    static String pathToCustomersPackage = "../../"+pathToFxmlPackage+"/content/customers";
    protected static Customer selectedCustomer = null;

    @FXML
    private StackPane stackPane;
    @FXML
    private JFXTreeTableView<Customer> customersTreeTable;
    @FXML
    private ObservableList<Customer> customersData;

    @Override
    public void initialize(URL url, ResourceBundle rb) { setTreeTable(); }

    /**
     * This method handles add button. It opens add customer page.
     * @param mouseEvent
     */
    @FXML
    private void handleAddButtonAction(MouseEvent mouseEvent) {
        openNewStackPage(stackPane, pathToCustomersPackage+"/create_customer.fxml");
    }

    /**
     * This method handles settings button and opens settings. But its not implemented yet.
     * @param mouseEvent
     */
    @FXML
    private void handleSettingsButtonAction(MouseEvent mouseEvent) { } //TODO FUTURE

    private HashMap<String, SimpleStringProperty> initValues() { //init HashMap to null for all attributes
        HashMap<String, SimpleStringProperty> values = new HashMap<>();
        for (CustomersAttribute attr : data.handleCustomersAttribute.getAttributes()) { values.put(attr.getName(), new SimpleStringProperty("")); }
        return values;
    }

    private void setTreeTable() {
        TreeTableColumn id = new TreeTableColumn("ID");
        id.setMinWidth(50);
        TreeTableColumn secondName = new TreeTableColumn("Příjmení");
        secondName.setMinWidth(175);
        TreeTableColumn firstName = new TreeTableColumn("Jméno");
        firstName.setMinWidth(125);
        TreeTableColumn phone = new TreeTableColumn("Telefon");
        phone.setMinWidth(125);
        TreeTableColumn notes = new TreeTableColumn("Poznámky");
        notes.setMinWidth(125);

        customersTreeTable.getColumns().addAll(id, secondName, firstName, phone, notes);

        try {
            customersData = data.handleCustomer.getAllCustomers();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // TODO FUTURE - nastaveni listview

        id.setCellValueFactory(new TreeItemPropertyValueFactory<Customer,String>("id"));
        firstName.setCellValueFactory(new TreeItemPropertyValueFactory<Customer,String>("firstName"));
        secondName.setCellValueFactory(new TreeItemPropertyValueFactory<Customer,String>("secondName"));
        phone.setCellValueFactory(new TreeItemPropertyValueFactory<Customer,String>("phone"));
        notes.setCellValueFactory(new TreeItemPropertyValueFactory<Customer,String>("notes"));

        final TreeItem<Customer> root = new RecursiveTreeItem<>(customersData, RecursiveTreeObject::getChildren);

        customersTreeTable.setRoot(root);
        customersTreeTable.setOnMouseClicked(event -> {
            if(event.getClickCount() == 2) {
                try {
                    selectedCustomer = customersTreeTable.getSelectionModel().getSelectedItem().getValue();
                    openNewStackPage(stackPane, pathToCustomersPackage+"/profile/customers_profile.fxml");
                } catch(Exception e) {}
            }
        });
        customersTreeTable.setShowRoot(false);
    }

    protected List<HBox> createFields(ScrollPane scrollPane) {
        VBox content = new VBox();
        List<HBox> infoLines = new ArrayList<>();
        addHboxWith("Jméno*:", "textfield", content, infoLines);
        addHboxWith("Příjmení*:", "textfield", content, infoLines);
        addHboxWith("Telefon*:", "textfield", content, infoLines);
        for (CustomersAttribute element : data.handleCustomersAttribute.getAttributes()) {
            String fieldName = element.getName();
            if(element.getMandatory()) { fieldName += "*"; }
            fieldName += ":";
            addHboxWith(fieldName, element.getType(), content, infoLines);
        }
        addHboxWith("Poznámky:", "textarea", content, infoLines);
        scrollPane.setContent(content);
        return infoLines;
    }

    protected Customer createCustomer(List<HBox> infoLines) {
        String firstName = "", secondName = "", notes = "", phone="";
        HashMap<String, SimpleStringProperty> values = initValues();

        for (HBox hbox : infoLines) {
            Node labelNode = hbox.getChildren().get(0);
            Node fieldNode = hbox.getChildren().get(1);
            String labelText = "";

            if(labelNode instanceof Label) { labelText = ((Label)labelNode).getText(); }
            if(labelText.contains("Jméno")) {
                if(fieldNode instanceof JFXTextField) { firstName = ((JFXTextField)fieldNode).getText(); }
            } else if(labelText.contains("Příjmení")) {
                if(fieldNode instanceof JFXTextField) { secondName = ((JFXTextField)fieldNode).getText(); }
            } else if(labelText.contains("Telefon")) {
                if(fieldNode instanceof JFXTextField) { phone = ((JFXTextField)fieldNode).getText(); }
            } else if(labelText.contains("Poznámky")) {
                if(fieldNode instanceof JFXTextArea) { notes = ((JFXTextArea)fieldNode).getText(); }
            } else {
                getUnknownLines(labelText, fieldNode, values);
            }
        }
        return new Customer(0, firstName, secondName, phone, notes, values);
    }

    private void getUnknownLines(String text, Node field, HashMap<String, SimpleStringProperty> values) {
        int textLength = text.length();
        String name, value = "";
        if(text.substring(textLength-2, textLength).equals("*:")) { name = text.substring(0, textLength-2); }
        else { name = text.substring(0, textLength-1); }
        if(field instanceof JFXTextField) { values.put(name, new SimpleStringProperty(((JFXTextField)field).getText())); }
        if(field instanceof JFXTextArea) { values.put(name, new SimpleStringProperty(((JFXTextArea)field).getText())); }
        // TODO FUTURE - dalsi typy pole
    }
}
