package main.presentation.java.content.customers.profile;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import main.presentation.java.content.customers.CustomersController;
import main.model.Customer;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.*;

/**
 * This controller handles customers profile generally. It loads tab panes.
 */
public class CustomersProfileController extends CustomersController {
    static String pathFromProfileToCustomerPackage = "../../../"+pathToFxmlPackage+"/content/customers";

    @FXML private CustomersProfileInfoController infoController;
    @FXML private CustomersProfileOrderController orderController;
    @FXML private CustomersProfileReminderController reminderController;

    @FXML
    private StackPane stackPane;
    @FXML
    private AnchorPane infoTabPane;
    @FXML
    private AnchorPane orderTabPane;
    @FXML
    private AnchorPane reminderTabPane;
    @FXML
    private Label nameLabel;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        infoController = new CustomersProfileInfoController();
        infoController.init(this);
        orderController = new CustomersProfileOrderController();
        orderController.init(this);
        reminderController = new CustomersProfileReminderController();
        reminderController.init(this);
        loadTabPane();
        if(selectedCustomer != null) {
            nameLabel.setText(selectedCustomer.getFirstName() + " " + selectedCustomer.getSecondName());
        } else {
            openUnderstandDialogWithText(stackPane, DIALOG_ERROR);
        }
    }

    /**
     * This method handles back button. It returns user to the overview.
     * @param mouseEvent
     */
    public void handleBackButton(MouseEvent mouseEvent) {
        openNewStackPage(stackPane, pathFromProfileToCustomerPackage+"/customers.fxml");
    }

    public StackPane getStackPane() { return stackPane; }

    private void loadTabPane() {
        openNewAnchorPage(infoTabPane, pathFromProfileToCustomerPackage+"/profile/customers_profile_info.fxml", infoController);
        openNewAnchorPage(orderTabPane, pathFromProfileToCustomerPackage+"/profile/customers_profile_order.fxml", orderController);
        openNewAnchorPage(reminderTabPane, pathFromProfileToCustomerPackage+"/profile/customers_profile_reminder.fxml", reminderController);
    }

    public void saveCustomer(MouseEvent mouseEvent, List<HBox> infoLines) {
        checkMandatoryFields(stackPane, infoLines);
    }

    public void openDeleteDialog(MouseEvent mouseEvent) {
        openDeleteYesNoDialogWithText(CUSTOMER_DELETE_DIALOG);
    }

    public void openUpdateDialog(MouseEvent mouseEvent) {
        openUnderstandDialogWithText(stackPane, CUSTOMER_UPDATE_DIALOG_NO_CHANGES);
    }

    public void openUpdateDialog(MouseEvent mouseEvent, Customer customer, Customer changes) {
        openUpdateYesNoDialogWithText(CUSTOMER_UPDATE_DIALOG, customer, changes);
    }

    private void updateCustomer(Customer customer, Customer changes) {
        try {
            data.handleCustomer.updateCustomer(customer, changes);
        } catch (SQLException e) {
            openUnderstandDialogWithText(stackPane, DIALOG_ERROR);
            e.printStackTrace();
            return;
        }
        selectedCustomer = customer;
    }

    private void deleteCustomer() {
        try {
            data.handleCustomer.deleteCustomer(selectedCustomer.getId());
        } catch (SQLException e) {
            openUnderstandDialogWithText(stackPane, DIALOG_ERROR);
            e.printStackTrace();
        }
    }

    private void openDeleteYesNoDialogWithText(String withBody) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setBody(new Text(withBody));

        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button1 = new JFXButton(DIALOG_YES);
        JFXButton button2 = new JFXButton(DIALOG_NO);
        button1.setOnAction(event -> {
            dialog.close();
            deleteCustomer();
            openNewStackPage(stackPane, pathFromProfileToCustomerPackage+"/customers.fxml");
            openUnderstandDialogWithText(stackPane, CUSTOMER_DELETE_DIALOG_OK);
        });
        button2.setOnAction(event -> dialog.close());
        content.setActions(button1,button2);
        dialog.show();
    }

    private void openUpdateYesNoDialogWithText(String withBody, Customer customer, Customer changes) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setBody(new Text(withBody));

        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button1 = new JFXButton(DIALOG_YES);
        JFXButton button2 = new JFXButton(DIALOG_NO);
        button1.setOnAction(event -> {
            dialog.close();
            updateCustomer(customer, changes);
            openUnderstandDialogWithText(stackPane, DIALOG_UPDATE_OK);
        });
        button2.setOnAction(event -> dialog.close());
        content.setActions(button1,button2);
        dialog.show();
    }
}
