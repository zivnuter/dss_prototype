package main.presentation.java.content.customers.profile;

import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import main.presentation.java.content.customers.CustomersController;
import main.model.Customer;
import main.model.CustomersAttribute;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.DIALOG_FILL_MANDATORY;
import static main.constants.GeneralConstants.STRING_FALSE;
import static main.constants.GeneralConstants.STRING_TRUE;

/**
 * This controller handles customer profil information tab pane.
 */
public class CustomersProfileInfoController extends CustomersController {
    public CustomersProfileController mainController;
    private List<HBox> infoLines;
    @FXML
    private ScrollPane scrollPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        infoLines = createFields(scrollPane);
        assert  selectedCustomer != null;
        loadInfo();
    }

    public void init(CustomersProfileController customersProfileController) {
        mainController = customersProfileController;
    }

    /**
     * This method handles save customer button. It updates current customer.
     * @param mouseEvent
     */
    public void saveCustomer(MouseEvent mouseEvent) {
        if(!checkMandatoryFields(mainController.getStackPane(), infoLines)) { return; }
        Customer customer = createCustomer(infoLines);
        customer.setId(selectedCustomer.getId());
        Customer changes = getChanges(customer);
        if( !selectedCustomer.equals(customer) ) {
            mainController.openUpdateDialog(mouseEvent, customer, changes);
        }
        else { mainController.openUpdateDialog(mouseEvent); }
    }

    /**
     * This method handles delete customer button. It deletes current customer.
     * @param mouseEvent
     */
    public void deleteCustomer(MouseEvent mouseEvent) {
        mainController.openDeleteDialog(mouseEvent);
    }

    private void loadInfo() {
        for (HBox hbox : infoLines) {
            loadLine(hbox);
        }
    }

    private void loadLine(HBox hbox) {
        Node labelNode = hbox.getChildren().get(0);
        Node fieldNode = hbox.getChildren().get(1);
        String labelText = "";

        if(labelNode instanceof Label) { labelText = ((Label)labelNode).getText(); }
        if(labelText.contains("Jméno")) {
            if(fieldNode instanceof JFXTextField) { ((JFXTextField)fieldNode).setText(selectedCustomer.getFirstName()); }
        } else if(labelText.contains("Příjmení")) {
            if(fieldNode instanceof JFXTextField) { ((JFXTextField)fieldNode).setText(selectedCustomer.getSecondName()); }
        } else if(labelText.contains("Telefon")) {
            if(fieldNode instanceof JFXTextField) { ((JFXTextField)fieldNode).setText(selectedCustomer.getPhone()); }
        } else if(labelText.contains("Poznámky")) {
            if(fieldNode instanceof JFXTextArea) { ((JFXTextArea)fieldNode).setText(selectedCustomer.getNotes()); }
        } else {
            loadUnknownLine(labelText, fieldNode);
        }
    }

    private void loadUnknownLine(String text, Node fieldNode) {
        int textLength = text.length();
        String name, value = "";
        if(text.substring(textLength-2, textLength).equals("*:")) { name = text.substring(0, textLength-2); }
        else { name = text.substring(0, textLength-1); }
        if(selectedCustomer.getValue(name) != null) { value = selectedCustomer.getValue(name); }

        for (CustomersAttribute attr : data.handleCustomersAttribute.getAttributes()) {
            if(text.contains(attr.getName())) {
                if(fieldNode instanceof JFXTextField) { ((JFXTextField)fieldNode).setText(value); }
                if(fieldNode instanceof JFXTextArea) { ((JFXTextArea)fieldNode).setText(value); }
            }
        }
    }

    private Customer getChanges(Customer customer) {
        HashMap<String, SimpleStringProperty>  values = new HashMap<>();
        String firstName = STRING_FALSE, secondName = STRING_FALSE, notes = STRING_FALSE, phone = STRING_FALSE;
        if(!selectedCustomer.getFirstName().equals(customer.getFirstName())) { firstName = STRING_TRUE; }
        if(!selectedCustomer.getSecondName().equals(customer.getSecondName())) { secondName = STRING_TRUE; }
        if(!selectedCustomer.getPhone().equals(customer.getPhone())) { phone = STRING_TRUE; }
        if(!selectedCustomer.getNotes().equals(customer.getNotes())) { notes = STRING_TRUE; }
        for (CustomersAttribute attr : data.handleCustomersAttribute.getAttributes()) {
            if(!selectedCustomer.getValue(attr.getName()).equals(customer.getValue(attr.getName()))) {
                values.put(attr.getName(), new SimpleStringProperty(STRING_TRUE));
            } else {
                values.put(attr.getName(), new SimpleStringProperty(STRING_FALSE));
            }
        }
        return new Customer(0, firstName, secondName, phone, notes, values );
    }
}
