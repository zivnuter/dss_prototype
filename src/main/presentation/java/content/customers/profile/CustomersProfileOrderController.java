package main.presentation.java.content.customers.profile;

import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import main.model.Order;
import main.presentation.java.content.customers.CustomersController;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * This controller handles customers orders overview tab pane.
 */
public class CustomersProfileOrderController extends CustomersProfileController {
    private CustomersProfileController mainController;
    private CustomersProfileOrderDetailController detailController;
    private Order selectedOrder;

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private JFXTreeTableView<Order> customerOrdersTreeTable;
    @FXML
    private ObservableList<Order> ordersData;

    @Override
    public void initialize(URL url, ResourceBundle rb) { setTreeTable(); }

    public void init(CustomersProfileController customersProfileController) {
        mainController = customersProfileController;
    }

    public StackPane getStackPane() {
        return mainController.getStackPane();
    }

    private void setTreeTable() {
        TreeTableColumn id = new TreeTableColumn("ID");
        id.setMinWidth(50);
        TreeTableColumn customer = new TreeTableColumn("Zákazník");
        customer.setMinWidth(175);
        TreeTableColumn price = new TreeTableColumn("Cena");
        price.setMinWidth(90);
        TreeTableColumn deliverDate = new TreeTableColumn("Datum doručení");
        deliverDate.setMinWidth(110);
        TreeTableColumn state = new TreeTableColumn("Stav");
        state.setMinWidth(100);
        TreeTableColumn notes = new TreeTableColumn("Poznámky");
        notes.setMinWidth(200);
        customerOrdersTreeTable.getColumns().addAll(id, customer, price, deliverDate, state, notes);

        try {
            ordersData = data.handleOrder.getOrdersForCustomer(selectedCustomer.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        id.setCellValueFactory(new TreeItemPropertyValueFactory<Order, String>("id"));
        customer.setCellValueFactory(new TreeItemPropertyValueFactory<Order, String>("customer"));
        price.setCellValueFactory(new TreeItemPropertyValueFactory<Order, String>("price"));
        deliverDate.setCellValueFactory(new TreeItemPropertyValueFactory<Order, String>("date"));
        state.setCellValueFactory(new TreeItemPropertyValueFactory<Order, String>("status"));
        notes.setCellValueFactory(new TreeItemPropertyValueFactory<Order, String>("notes"));

        final TreeItem<Order> root = new RecursiveTreeItem<>(ordersData, RecursiveTreeObject::getChildren);

        customerOrdersTreeTable.setRoot(root);
        customerOrdersTreeTable.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                try {
                    selectedOrder = customerOrdersTreeTable.getSelectionModel().getSelectedItem().getValue();
                    detailController = new CustomersProfileOrderDetailController();
                    openNewAnchorPage(anchorPane, pathFromProfileToCustomerPackage + "/profile/customers_profile_order_detail.fxml", detailController);
                    detailController.init(this, selectedOrder, pathFromProfileToCustomerPackage);
                } catch (Exception e) {
                }
            }
        });
        customerOrdersTreeTable.setShowRoot(false);
    }
}
