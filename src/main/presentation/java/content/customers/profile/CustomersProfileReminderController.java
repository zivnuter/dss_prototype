package main.presentation.java.content.customers.profile;

import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import main.model.Reminder;

import java.net.URL;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.PROBLEM_WITH_REASONS;

/**
 * This controller handles reminder tab pane in customers profile.
 */
public class CustomersProfileReminderController extends CustomersProfileController {
    private CustomersProfileController mainController;
    private CustomersProfileReminderCreateController createController;
    private CustomersProfileReminderDetailController detailController;
    static Reminder selectedReminder;

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private JFXTreeTableView<Reminder> remindersTreeTable;
    @FXML
    private ObservableList<Reminder> remindersData;

    @Override
    public void initialize(URL url, ResourceBundle rb) { setTreeTable(); }

    public void init(CustomersProfileController customersProfileController) {
        mainController = customersProfileController;
    }

    /**
     * This method handles add button. It opens the add reminder page.
     * @param mouseEvent
     */
    @FXML
    private void handleAddButtonAction(MouseEvent mouseEvent) {
        createController = new CustomersProfileReminderCreateController();
        openNewAnchorPage(anchorPane, pathFromProfileToCustomerPackage+"/profile/customers_profile_reminder_create.fxml", createController);
        createController.init(this);
    }

    public StackPane getStackPane() {
        return mainController.getStackPane();
    }

    Reminder createReminder(List<HBox> infoLines) {
        int customerId = selectedCustomer.getId();
        String reason = "", notes = "";
        Date date = null;

        for (HBox hbox : infoLines) {
            Node nodeLabel = hbox.getChildren().get(0);
            Node nodeField = hbox.getChildren().get(1);
            String labelText = "";
            if (nodeLabel instanceof Label) {
                labelText = ((Label) nodeLabel).getText();
            }

            if (labelText.contains("Důvod upomínky")) {
                if (nodeField instanceof JFXComboBox) {
                    reason = ((JFXComboBox<String>) nodeField).getValue();
                }
            } else if (labelText.contains("Datum")) {
                if (nodeField instanceof JFXDatePicker) {
                    LocalDate localDate = ((JFXDatePicker) nodeField).getValue();
                    if (localDate != null) {
                        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
                        date = Date.from(instant);
                    }
                }
            } else if (labelText.contains("Poznámky")) {
                if (nodeField instanceof JFXTextArea) {
                    notes = ((JFXTextArea) nodeField).getText();
                }
            }
        }
        return new Reminder(0, customerId, reason, date, notes);
    }

    protected List<HBox> createFields(ScrollPane scrollPane) {
        VBox content = new VBox();
        List<HBox> infoLines = new ArrayList<>();
        String[] comboBoxText = new String[0];
        try {
            comboBoxText = data.handleReason.getAllReasons();
        } catch (SQLException e) {
            openUnderstandDialogWithText(mainController.getStackPane(), PROBLEM_WITH_REASONS);
            e.printStackTrace();
        }
        addHboxWith("Důvod upomínky*:", content, infoLines, comboBoxText);
        addHboxWith("Datum*:", "datepicker", content, infoLines);
        addHboxWith("Poznámky:", "textarea", content, infoLines);
        scrollPane.setContent(content);
        return infoLines;
    }

    private void setTreeTable() {
        TreeTableColumn id = new TreeTableColumn("ID");
        id.setMinWidth(50);
        TreeTableColumn reason = new TreeTableColumn("Důvod");
        reason.setMinWidth(100);
        TreeTableColumn date = new TreeTableColumn("Datum");
        date.setMinWidth(110);
        TreeTableColumn notes = new TreeTableColumn("Poznámky");
        notes.setMinWidth(250);
        remindersTreeTable.getColumns().addAll(id, reason, date, notes);

        try {
            remindersData = data.handleReminder.getAllReminders(selectedCustomer.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        id.setCellValueFactory(new TreeItemPropertyValueFactory<Reminder,String>("id"));
        reason.setCellValueFactory(new TreeItemPropertyValueFactory<Reminder,String>("reason"));
        date.setCellValueFactory(new TreeItemPropertyValueFactory<Reminder,String>("date"));
        notes.setCellValueFactory(new TreeItemPropertyValueFactory<Reminder,String>("notes"));

        final TreeItem<Reminder> root = new RecursiveTreeItem<>(remindersData, RecursiveTreeObject::getChildren);

        remindersTreeTable.setRoot(root);
        remindersTreeTable.setOnMouseClicked(event -> {
            if(event.getClickCount() == 2) {
                try {
                    selectedReminder = remindersTreeTable.getSelectionModel().getSelectedItem().getValue();
                    detailController = new CustomersProfileReminderDetailController();
                    openNewAnchorPage(anchorPane, pathFromProfileToCustomerPackage+"/profile/customers_profile_reminder_detail.fxml", detailController);
                    detailController.init(this);
                } catch(Exception e) {}
            }
        });
        remindersTreeTable.setShowRoot(false);
    }
}
