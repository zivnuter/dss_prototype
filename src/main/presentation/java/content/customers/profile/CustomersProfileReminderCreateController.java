package main.presentation.java.content.customers.profile;

import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.DIALOG_ERROR;
import static main.constants.DialogContants.REMINDERS_CREATE_OK;

/**
 * This controller handles add reminder page.
 */
public class CustomersProfileReminderCreateController extends CustomersProfileReminderController {
    private CustomersProfileReminderController mainController;
    private List<HBox> infoLines;

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ScrollPane scrollPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        infoLines = createFields(scrollPane);
    }

    public void init(CustomersProfileReminderController mainController) {
        this.mainController = mainController;
    }

    /**
     * This method handles back button. It returns user to the reminders overview.
     * @param mouseEvent
     */
    @FXML
    private void back(javafx.scene.input.MouseEvent mouseEvent) {
        openNewAnchorPage(anchorPane, pathFromProfileToCustomerPackage+"/profile/customers_profile_reminder.fxml", mainController);
    }

    /**
     * This method handles create button. It creates the reminder.
     * @param mouseEvent
     */
    @FXML
    private void create(javafx.scene.input.MouseEvent mouseEvent) {
        if(!checkMandatoryFields(mainController.getStackPane(), infoLines)) {
            return;
        }
        else {
            try {
                data.handleReminder.insertReminder(createReminder(infoLines));
                openUnderstandDialogWithText(mainController.getStackPane(), REMINDERS_CREATE_OK);
                openNewAnchorPage(anchorPane, pathFromProfileToCustomerPackage+"/profile/customers_profile_reminder.fxml", mainController);
            } catch (SQLException e) {
                openUnderstandDialogWithText(mainController.getStackPane(), DIALOG_ERROR);
                e.printStackTrace();
            }
        }
    }
}
