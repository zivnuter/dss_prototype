package main.presentation.java.content.customers.profile;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import main.model.Reminder;

import java.net.URL;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.*;

/**
 * This controller handles reminder detail page in customers profile.
 */
public class CustomersProfileReminderDetailController extends CustomersProfileReminderController {
    private CustomersProfileReminderController mainController;
    private List<HBox> infoLines;

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ScrollPane scrollPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        infoLines = createFields(scrollPane);
        fillData();
    }

    public void init(CustomersProfileReminderController mainController) {
        this.mainController = mainController;
    }

    /**
     * This method handles back button. It returns user to the reminders overview.
     * @param mouseEvent
     */
    @FXML
    private void back(javafx.scene.input.MouseEvent mouseEvent) {
        openNewAnchorPage(anchorPane, pathFromProfileToCustomerPackage+"/profile/customers_profile_reminder.fxml", mainController);
    }

    /**
     * This method handles save button. It updates current reminder.
     * @param mouseEvent
     */
    @FXML
    private void save(javafx.scene.input.MouseEvent mouseEvent) {
        if(!checkMandatoryFields(mainController.getStackPane(), infoLines)){ return; }
        Reminder reminder = createReminder(infoLines);
        reminder.setId(selectedReminder.getId());
        if(selectedReminder.equals(reminder)) {
            openUnderstandDialogWithText(mainController.getStackPane(), DIALOG_NO_CHANGES);
            return;
        }
        try {
            data.handleReminder.updateReminder(reminder);
            openUnderstandDialogWithText(mainController.getStackPane(), DIALOG_UPDATE_OK);
        } catch (SQLException e) {
            openUnderstandDialogWithText(mainController.getStackPane(), DIALOG_ERROR);
            e.printStackTrace();
        }

    }

    /**
     * This method handles delete button. It deletes the current reminder.
     * @param mouseEvent
     */
    @FXML
    private void delete(javafx.scene.input.MouseEvent mouseEvent) {
        openDeleteYesNoDialogWithText(REMINDERS_CREATE_DELETE);
    }

    private void deleteReminder() {
        try {
            data.handleReminder.deleteReminder(selectedReminder.getId());
            openUnderstandDialogWithText(mainController.getStackPane(), REMINDERS_CREATE_DELETE_OK);
            openNewAnchorPage(anchorPane, pathFromProfileToCustomerPackage+"/profile/customers_profile_reminder.fxml", mainController);

        } catch (SQLException e) {
            openUnderstandDialogWithText(mainController.getStackPane(), DIALOG_ERROR);
            e.printStackTrace();
        }
    }

    private void openDeleteYesNoDialogWithText(String withBody) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setBody(new Text(withBody));

        JFXDialog dialog = new JFXDialog(mainController.getStackPane(), content, JFXDialog.DialogTransition.CENTER);
        JFXButton button1 = new JFXButton(DIALOG_YES);
        JFXButton button2 = new JFXButton(DIALOG_NO);
        button1.setOnAction(event -> {
            dialog.close();
            deleteReminder();
        });
        button2.setOnAction(event -> dialog.close());
        content.setActions(button1,button2);
        dialog.show();
    }

    private void fillData() {
        for (HBox hbox : infoLines) {
            Node nodeLabel = hbox.getChildren().get(0);
            Node nodeField = hbox.getChildren().get(1);
            String labelText = "";
            if(nodeLabel instanceof Label) { labelText = ((Label)nodeLabel).getText(); }

            if(labelText.contains("Důvod")) {
                if(nodeField instanceof JFXComboBox) {
                    ((JFXComboBox)nodeField).setValue(selectedReminder.getReason());
                }
            } else if(labelText.contains("Datum")) {
                if(nodeField instanceof JFXDatePicker) {
                    LocalDate localDate = Instant.ofEpochMilli(selectedReminder.getDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
                    ((JFXDatePicker)nodeField).setValue(localDate);
                }
            } else if(labelText.contains("Poznámky")) {
                if(nodeField instanceof JFXTextArea) {
                    ((JFXTextArea)nodeField).setText(selectedReminder.getNotes());
                }
            }
        }
    }
}
