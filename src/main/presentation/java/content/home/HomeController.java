package main.presentation.java.content.home;

import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import main.model.Order;
import main.presentation.java.CommonController;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * This controller handles home page.
 */
public class HomeController extends CommonController {
    @FXML
    private JFXTreeTableView<Order> customerOrdersTreeTable;
    @FXML
    private ObservableList<Order> ordersData;

    @Override
    public void initialize(URL url, ResourceBundle rb) { setTreeTable(); }

    /**
     * This method handles settings button. Its not implemented yet.
     * @param mouseEvent
     */
    public void handleSettings(MouseEvent mouseEvent) { }

    private void setTreeTable() {
        TreeTableColumn id = new TreeTableColumn("ID");
        id.setMinWidth(50);
        TreeTableColumn customer = new TreeTableColumn("Zákazník");
        customer.setMinWidth(175);
        TreeTableColumn deliverDate = new TreeTableColumn("Datum doručení");
        deliverDate.setMinWidth(110);
        TreeTableColumn state = new TreeTableColumn("Stav");
        state.setMinWidth(100);
        TreeTableColumn notes = new TreeTableColumn("Poznámky");
        notes.setMinWidth(250);
        customerOrdersTreeTable.getColumns().addAll(id, customer, deliverDate, state, notes);

        try {
            ordersData = data.handleOrder.getOrdersToExecute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        id.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("id"));
        customer.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("customer"));
        deliverDate.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("date"));
        state.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("status"));
        notes.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("notes"));

        final TreeItem<Order> root = new RecursiveTreeItem<>(ordersData, RecursiveTreeObject::getChildren);

        customerOrdersTreeTable.setRoot(root);
        customerOrdersTreeTable.setShowRoot(false);
    }
}
