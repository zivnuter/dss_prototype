package main.presentation.java.content.orders;

import com.jfoenix.controls.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import main.model.Customer;
import main.model.Order;
import main.model.OrderPart;
import main.model.Product;
import main.presentation.java.CommonController;
import main.presentation.java.content.orders.customersorders.CustomersOrdersController;
import main.presentation.java.content.orders.toexecute.OrdersToExecuteController;

import java.net.URL;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.NO_PRODUCTS;
import static main.constants.DialogContants.PRODUCTS_MANDATORY;

/**
 * This controller handles orders generally. It loads all tab panes.
 */
public class OrdersController extends CommonController{
    protected static String path = "../../"+pathToFxmlPackage+"/content/orders";

    @FXML
    private CustomersOrdersController customersOrdersController;
    @FXML
    private OrdersToExecuteController ordersToExecuteController;

    @FXML
    private AnchorPane ordersToExecuteTab;
    @FXML
    private AnchorPane customersOrdersTab;
    @FXML
    private StackPane stackPane;

    /**
     * This method lads order tab panes.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        customersOrdersController = new CustomersOrdersController();
        customersOrdersController.init(this);
        ordersToExecuteController = new OrdersToExecuteController();
        ordersToExecuteController.init(this);
        loadTabPane();
    }

    public StackPane getStackPane() { return stackPane; }

    public void reloadOrdersToExecute() {
        ordersToExecuteController.reloadTreeTable();
    }

    public void reloadCustomersOrders() {
        customersOrdersController.reloadTreeTable();
    }

    private void loadTabPane() {
        openNewAnchorPage(customersOrdersTab, path+"/customerorders/customers_orders.fxml", customersOrdersController);
        openNewAnchorPage(ordersToExecuteTab, path+"/toexecute/to_execute.fxml", ordersToExecuteController);
    }

    protected Order createOrderObject(List<HBox> infoLines, double price) {
        int customerId = 0;
        String status = "", notes = "";
        Date date = null;

        for (HBox hbox : infoLines) {
            Node nodeLabel = hbox.getChildren().get(0);
            Node nodeField = hbox.getChildren().get(1);
            String labelText = "";
            if(nodeLabel instanceof Label) { labelText = ((Label)nodeLabel).getText(); }

            if(labelText.contains("Zákazník")) {
                if(nodeField instanceof JFXComboBox) {
                    Customer c = ((JFXComboBox<Customer>)nodeField).getSelectionModel().getSelectedItem();
                    customerId = c.getId();
                }
            } else if(labelText.contains("Stav")) {
                if(nodeField instanceof JFXComboBox) {
                    status = ((JFXComboBox<String>)nodeField).getValue();
                    System.out.println(status);
                }
            } else if(labelText.contains("Datum")) {
                if(nodeField instanceof JFXDatePicker) {
                    LocalDate localDate = ((JFXDatePicker)nodeField).getValue();
                    if(localDate != null) {
                        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
                        date = Date.from(instant);
                    }
                }
            } else if(labelText.contains("Poznámky")) {
                if(nodeField instanceof JFXTextArea) {
                    notes = ((JFXTextArea)nodeField).getText();
                }
            }
        }
        return new Order(0, date, status,price, null, notes, null, customerId);
    }

    protected List<OrderPart> createOrderPartList(List<HBox> productLines, int orderId) {
        List<OrderPart> orderParts = new ArrayList<>();

        for (HBox hbox : productLines) {
            int count = 0, productId = 0;
            String productName = "";
            double price = 0, endurance = 0;

            Node nodeComboBox = hbox.getChildren().get(0);
            Node nodeCount = hbox.getChildren().get(1);
            Node nodePrice = hbox.getChildren().get(3);

            if(nodeComboBox instanceof JFXComboBox) {
                Product p = ((JFXComboBox<Product>)nodeComboBox).getSelectionModel().getSelectedItem();
                productId = p.getId();
                productName = p.getName();
                endurance = p.getEndurance();
            }
            if(nodeCount instanceof JFXTextField) {
                count = Integer.parseInt(((JFXTextField)nodeCount).getText());
            }
            if(nodePrice instanceof JFXTextField) {
                price = Double.parseDouble(((JFXTextField)nodePrice).getText());
            }
            orderParts.add(new OrderPart(0, count, productName, price, orderId, productId)); //TODO FUTURE -  endurance
        }

        return orderParts;
    }

    protected List<HBox> createFields(ScrollPane scrollPane) {
        VBox content = new VBox();
        List<HBox> infoLines = new ArrayList<>();
        String[] statusComboBoxText = {"doručeno", "doručit", "objednat"};
        addCustomersComboBox("Zákazník*:", content, infoLines);
        addHboxWith("Stav objednávky*:", content, infoLines, statusComboBoxText);
        addHboxWith("Datum doručení:", "datepicker", content, infoLines);
        addHboxWith("Poznámky:", "textarea", content, infoLines);
        scrollPane.setContent(content);

        return infoLines;
    }

    private void addCustomersComboBox(String text, VBox content, List<HBox> infoLines) {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(5, 0, 5, 0));
        hbox.setAlignment(Pos.TOP_RIGHT);
        Label label = new Label(text);
        label.setPadding(new Insets(0, 10, 0, 10));
        ObservableList<Customer> allCustomers = FXCollections.observableArrayList();
        try {
            allCustomers = data.handleCustomer.getAllCustomersWithNullValues();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        JFXComboBox<Customer> customers = new JFXComboBox<>(allCustomers);
        customers.setMinWidth(400);
        customers.setStyle("-fx-background-color: white; -jfx-unfocus-color: transparent; -jfx-focus-color: #4e8397;");
        Callback<ListView<Customer>, ListCell<Customer>> factory = lv -> new ListCell<>() {
            @Override
            protected void updateItem(Customer item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getId()+" - "+item.getSecondName()+" "+item.getFirstName());
            }
        };
        customers.setCellFactory(factory);
        customers.setButtonCell(factory.call(null));
        hbox.getChildren().addAll(label, customers);
        content.getChildren().add(hbox);
        infoLines.add(hbox);
    }

    protected boolean checkProducts(StackPane stackPane, List<HBox> productLines) {
        boolean isSomeMissing = false;
        if(productLines.size() == 0) {
            openUnderstandDialogWithText(stackPane, NO_PRODUCTS);
            return false;
        }
        for (HBox line : productLines) {
            Node nodeComboBox = line.getChildren().get(0);
            Node nodeCountField = line.getChildren().get(1);
            Node nodeDefPriceField = line.getChildren().get(2);
            Node nodePriceField = line.getChildren().get(3);

            if(nodeComboBox instanceof JFXComboBox) {
                if(((JFXComboBox)nodeComboBox).getSelectionModel().getSelectedItem() == null) { isSomeMissing = true; }
            }
            if(nodeCountField instanceof JFXTextField) {
                String text = ((JFXTextField)nodeCountField).getText();
                if(text.equals("")) { isSomeMissing = true;}
                if(!text.matches("\\d*")) { isSomeMissing = true; }
            }
            if(nodeDefPriceField instanceof JFXTextField) {
                String text = ((JFXTextField)nodeDefPriceField).getText();
                if(text.equals("")) { isSomeMissing = true;}
                if(!text.matches("\\d*.\\d*")) { isSomeMissing = true; }
            }
            if(nodePriceField instanceof JFXTextField) {
                String text = ((JFXTextField)nodePriceField).getText();
                if(text.equals("")) { isSomeMissing = true;}
                if(!text.matches("\\d*.\\d*")) { isSomeMissing = true; }
            }
        }
        if(isSomeMissing) {
            openUnderstandDialogWithText(stackPane, PRODUCTS_MANDATORY);
            return false;
        }
        return true;
    }

    protected void createProductLine(ScrollPane scrollPane, VBox content, List<HBox> productLines, Order objPrice, Label price) {
        HBox hbox = new HBox();
        JFXComboBox<Product> comboBox;
        hbox.setAlignment(Pos.TOP_RIGHT);
        comboBox = addProductsComboBox(hbox);
        JFXTextField countField = addTextField(hbox, "počet kusů");
        JFXTextField defPriceField = addTextField(hbox, "cena za kus");
        JFXTextField priceField = addTextField(hbox, "cena celkem");
        priceField.setDisable(true);
        priceField.textProperty().addListener((observable, oldp, newp) -> {
            double sumPrice = objPrice.getPrice();
            double oldprice = 0, newPrice = 0;
            if(!oldp.equals("")) {
                try { oldprice = Double.parseDouble(oldp); }
                catch(Exception e) {}
            }
            try { newPrice = Double.parseDouble(newp); }
            catch(Exception e) {}
            objPrice.setPrice(sumPrice - oldprice + newPrice);
            price.setText(Double.toString(objPrice.getPrice()));
        });
        countField.textProperty().addListener((observable, oldc, newc) -> {
            double newcount = 0, defPrice = 0;
            if(!newc.equals("")) {
                try { newcount = Double.parseDouble(newc); }
                catch(Exception e) {}
            }
            if(!defPriceField.getText().equals("")) {
                try { defPrice = Double.parseDouble(defPriceField.getText()); }
                catch(Exception e) {}
            }
            priceField.setText(Double.toString(newcount*defPrice));
        });
        defPriceField.textProperty().addListener((observable, oldp, newp) -> {
            double count = 0, defPrice = 0;
            if(!newp.equals("")) {
                try { defPrice = Double.parseDouble(newp); }
                catch(Exception e) {}
            }
            if(!countField.getText().equals("")) {
                try { count = Double.parseDouble(countField.getText()); }
                catch(Exception e) {}
            }
            priceField.setText(Double.toString(count*defPrice));
        });
        comboBox.valueProperty().addListener(new ChangeListener<Product>() {
            @Override public void changed(ObservableValue ov, Product previous, Product actual) {
                defPriceField.setText(Double.toString(actual.getPrice()));
            }
        });
        addButton(hbox, "Smazat", productLines, content, countField);
//        addButton(hbox, "Objednat", productLines); //TODO FUTURE - pridani do predpripravene objednavky
        content.getChildren().add(hbox);
        scrollPane.setContent(content);
        productLines.add(hbox);
    }

    private void deleteProductLine(HBox hbox, List<HBox> productLines, VBox content) {
        productLines.remove(hbox);
        content.getChildren().clear();
        content.getChildren().addAll(productLines);
    }

    private void addButton(HBox hbox, String text, List<HBox> productLines, VBox content, JFXTextField countField) {
        JFXButton button = new JFXButton();
         button.setContentDisplay(ContentDisplay.CENTER);
         button.setText(text);
         button.setPadding(new Insets(5));
         button.setMinSize(80, 35);
         button.setStyle("-fx-background-color: #4e8397; -fx-border-insets: 5px; -fx-background-insets: 5px;" +
                 "-fx-text-fill: white;");
         button.setOnAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent event) {
                 countField.setText("0");
                 if(text.equals("Smazat")) { deleteProductLine(hbox, productLines, content); }
//                 else {  } //TODO FUTURE - pridani do predpripravene objednavky
             }
         });
         hbox.getChildren().add(button);
    }

    private JFXTextField addTextField(HBox hbox, String promptText) {
        JFXTextField textField = new JFXTextField();
        textField.setPromptText(promptText);
        textField.setPadding(new Insets(5));
        textField.setStyle("-fx-background-color: white; -jfx-unfocus-color: transparent; -jfx-focus-color: #4e8397;" +
                "-fx-border-insets: 5px; -fx-background-insets: 5px;");
        textField.setMaxSize(100, 35);
        textField.setMinSize(100, 35);
        hbox.getChildren().add(textField);
        return textField;
    }

    private JFXComboBox addProductsComboBox(HBox hbox) {
        ObservableList<Product> allProducts = FXCollections.observableArrayList();
        try {
            allProducts = data.handleProduct.getAllProducts();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        JFXComboBox<Product> products = new JFXComboBox<>(allProducts);
        products.setMinWidth(250);
        products.setPadding(new Insets(5));
        products.setStyle("-fx-background-color: white; -jfx-unfocus-color: transparent; -jfx-focus-color: #4e8397;" +
                "-fx-border-insets: 5px; -fx-background-insets: 5px;");
        Callback<ListView<Product>, ListCell<Product>> factory = lv -> new ListCell<>() {
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getId()+" - "+item.getName());
            }
        };
        products.setCellFactory(factory);
        products.setButtonCell(factory.call(null));
        hbox.getChildren().add(products);
        return products;
    }

    protected void fillInfoLines(List<HBox> infoLines, Order order) throws SQLException {
        for (HBox hbox : infoLines) {
            Node nodeLabel = hbox.getChildren().get(0);
            Node nodeField = hbox.getChildren().get(1);
            String labelText = "";
            if(nodeLabel instanceof Label) { labelText = ((Label)nodeLabel).getText(); }

            if(labelText.contains("Zákazník")) {
                if(nodeField instanceof JFXComboBox) {
                    Customer c = data.handleCustomer.getCustomerById(order.getCustomersId());
                    ((JFXComboBox<Customer>)nodeField).getSelectionModel().select(c);
                }
            } else if(labelText.contains("Stav")) {
                if(nodeField instanceof JFXComboBox) {
                    ((JFXComboBox)nodeField).setValue(order.getStatus());
                }
            } else if(labelText.contains("Datum")) {
                if(nodeField instanceof JFXDatePicker) {
                    if(order.getDate() != null) {
                        LocalDate localDate = Instant.ofEpochMilli(order.getDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
                        ((JFXDatePicker)nodeField).setValue(localDate);
                    }
                }
            } else if(labelText.contains("Poznámky")) {
                if(nodeField instanceof JFXTextArea) {
                    ((JFXTextArea)nodeField).setText(order.getNotes());
                }
            }
        }
    }
}
