package main.presentation.java.content.orders.customersorders;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import main.model.Order;
import main.model.OrderPart;
import main.model.Reminder;
import main.presentation.java.content.orders.OrdersController;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.DIALOG_ERROR;
import static main.constants.DialogContants.ORDER_CREATE_OK;

/**
 * This controller handles create customers order page.
 */
public class CreateCustomersOrderController extends OrdersController {
    private CustomersOrdersController mainController;
    private List<HBox> infoLines;
    private List<HBox> productLines;
    private VBox content;
    private Order orderWithSumPrice;

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private ScrollPane productsScrollPane;
    @FXML
    private Label price;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        price.setText("0.0");
        orderWithSumPrice = new Order(0);
        productLines = new ArrayList<>();
        infoLines = createFields(scrollPane);
        content = new VBox();
    }

    public void init(CustomersOrdersController controller) {
        mainController = controller;
    }

    /**
     * This method handles back button. It returns user to the overview.
     * @param mouseEvent
     */
    public void handleBackButton(MouseEvent mouseEvent) {
        openNewAnchorPage(anchorPane, "../"+path+"/customerorders/customers_orders.fxml", mainController);
    }

    /**
     * This method handles add product button. It adds product line to order detail.
     * @param mouseEvent
     */
    public void addProduct(MouseEvent mouseEvent) {
        createProductLine(productsScrollPane, content, productLines, orderWithSumPrice, price);
    }

    /**
     * This method handles create button. It creates the order.
     * @param mouseEvent
     */
    public void create(MouseEvent mouseEvent) {
        if(!checkMandatoryFields(mainController.ordersController.getStackPane(), infoLines)) { return; }
        else if(!checkProducts(mainController.ordersController.getStackPane(), productLines)) { return; }
        else {
            try {
                int id = data.handleOrder.insertOrder(createOrderObject(infoLines, orderWithSumPrice.getPrice()));
                data.handleOrderPart.insertOrderParts(createOrderPartList(productLines, id));
                openUnderstandDialogWithText(mainController.ordersController.getStackPane(), ORDER_CREATE_OK);
                openNewAnchorPage(anchorPane, "../" + path + "/customerorders/customers_orders.fxml", mainController);
                mainController.reloadOrdersToExecute();
            } catch (SQLException e) {
                openUnderstandDialogWithText(mainController.ordersController.getStackPane(), DIALOG_ERROR);
                e.printStackTrace();
            }
        }
    }
}
