package main.presentation.java.content.orders.customersorders;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import main.model.Order;
import main.model.OrderPart;
import main.model.Product;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.*;

/**
 * This controller handles customers order detail page.
 */
public class CustomersOrderDetailController extends CustomersOrdersController {
    private CustomersOrdersController mainController;
    private List<HBox> infoLines;
    private List<HBox> productLines;
    private VBox content;
    private Order orderWithSumPrice;

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private ScrollPane productsScrollPane;
    @FXML
    private Label price;

    /**
     * This method loads info to the form about the selected order.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        price.setText(Double.toString(selectedOrder.getPrice()));
        orderWithSumPrice = new Order(0);
        infoLines = createFields(scrollPane);
        try {
            fillInfoLines(infoLines, selectedOrder);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        content = new VBox();
        loadProductLines();

    }

    public void init(CustomersOrdersController controller) {
        mainController = controller;
    }

    /**
     * This method handles update page. Its not implemented yet.
     * @param mouseEvent
     */
    public void update(MouseEvent mouseEvent) {
        openUnderstandDialogWithText(mainController.getStackPane(), NOT_IMPLEMENTED);
    }

    /**
     * This method handles add product button. It add product line to the form.
     * @param mouseEvent
     */
    public void addProduct(MouseEvent mouseEvent) {
        createProductLine(productsScrollPane, content, productLines, orderWithSumPrice, price);
    }

    /**
     * This method handles delete button. It deletes the current order.
     * @param mouseEvent
     */
    public void delete(MouseEvent mouseEvent) {
        openDeleteYesNoDialogWithText(ORDER_DELETE_PROMPT);
    }

    /**
     * This method handles back button. It returns user to the overview.
     * @param mouseEvent
     */
    public void handleBackButton(MouseEvent mouseEvent) {
        openNewAnchorPage(anchorPane, "../"+path+"/customerorders/customers_orders.fxml", mainController);
    }

    private void loadProductLines() {
        productLines = new ArrayList<>();

        for (OrderPart part : selectedOrder.getItems()) {
            createProductLine(productsScrollPane, content, productLines, orderWithSumPrice, price);
            int size = productLines.size();
            Node nodeComboBox = productLines.get(size-1).getChildren().get(0);
            Node nodeCountField = productLines.get(size-1).getChildren().get(1);
            Node nodePriceField = productLines.get(size-1).getChildren().get(2);

            if(nodeComboBox instanceof JFXComboBox) {
                Product product = null;
                try {
                    product = data.handleProduct.getProductsById(part.getFkProduct());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                ((JFXComboBox<Product>)nodeComboBox).getSelectionModel().select(product);
            }
            if(nodeCountField instanceof JFXTextField) {
                ((JFXTextField)nodeCountField).setText(Integer.toString(part.getCount()));
            }
            if(nodePriceField instanceof JFXTextField) {
                ((JFXTextField)nodePriceField).setText(Double.toString(part.getPrice()/part.getCount()));
            }
        }
    }

    private void deleteOrder() {
        try {
            data.handleOrderPart.deleteOrderParts(selectedOrder.getId());
            data.handleOrder.deleteOrder(selectedOrder.getId());
            openUnderstandDialogWithText(mainController.ordersController.getStackPane(), ORDER_DELETE_OK);
            openNewAnchorPage(anchorPane, "../"+path+"/customerorders/customers_orders.fxml", mainController);
            mainController.reloadOrdersToExecute();

            openNewAnchorPage(anchorPane, "../"+path+"/customerorders/customers_orders.fxml", mainController);
        } catch (SQLException e) {
            openUnderstandDialogWithText(mainController.ordersController.getStackPane(), DIALOG_ERROR);
            e.printStackTrace();
        }
    }

    private void openDeleteYesNoDialogWithText(String withBody) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setBody(new Text(withBody));

        JFXDialog dialog = new JFXDialog(mainController.ordersController.getStackPane(), content, JFXDialog.DialogTransition.CENTER);
        JFXButton button1 = new JFXButton(DIALOG_YES);
        JFXButton button2 = new JFXButton(DIALOG_NO);
        button1.setOnAction(event -> {
            dialog.close();
            deleteOrder();
        });
        button2.setOnAction(event -> dialog.close());
        content.setActions(button1,button2);
        dialog.show();
    }
}
