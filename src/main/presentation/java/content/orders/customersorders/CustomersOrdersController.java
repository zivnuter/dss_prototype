package main.presentation.java.content.orders.customersorders;

import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import main.model.Customer;
import main.model.Order;
import main.presentation.java.content.orders.OrdersController;
import main.presentation.java.content.orders.toexecute.OrdersToExecuteController;

import java.net.URL;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.ResourceBundle;

/**
 * This method handles customers orders tab pane and their show overview.
 */
public class CustomersOrdersController extends OrdersController {
    OrdersController ordersController;
    private CreateCustomersOrderController createOrderController;
    private CustomersOrderDetailController orderDetailController;
    static Order selectedOrder = null;

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private JFXTreeTableView<Order> customerOrdersTreeTable;
    @FXML
    private ObservableList<Order> ordersData;

    @Override
    public void initialize(URL url, ResourceBundle rb) { setTreeTable(); }

    public void init(OrdersController controller) { ordersController = controller; }

    /**
     * This method handles add button. It opens add order page.
     * @param mouseEvent
     */
    public void handleAddButton(MouseEvent mouseEvent) {
        createOrderController = new CreateCustomersOrderController();
        openNewAnchorPage(anchorPane, "../"+path+"/customerorders/create_customers_order.fxml", createOrderController);
        createOrderController.init(this);
    }

    public StackPane getStackPane() {
        return ordersController.getStackPane();
    }

    public void reloadOrdersToExecute() {
        ordersController.reloadOrdersToExecute();
    }

    public void reloadTreeTable() {
        customerOrdersTreeTable.getColumns().clear();
        setTreeTable();
    }

    private void setTreeTable() {
        TreeTableColumn id = new TreeTableColumn("ID");
        id.setMinWidth(50);
        TreeTableColumn customer = new TreeTableColumn("Zákazník");
        customer.setMinWidth(175);
        TreeTableColumn price = new TreeTableColumn("Cena");
        price.setMinWidth(110);
        TreeTableColumn deliverDate = new TreeTableColumn("Datum doručení");
        deliverDate.setMinWidth(110);
        TreeTableColumn state = new TreeTableColumn("Stav");
        state.setMinWidth(100);
        TreeTableColumn notes = new TreeTableColumn("Poznámky");
        notes.setMinWidth(250);
        customerOrdersTreeTable.getColumns().addAll(id, customer, price, deliverDate, state, notes);

        try {
            ordersData = data.handleOrder.getOrders();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        id.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("id"));
        customer.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("customer"));
        price.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("price"));
        deliverDate.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("date"));
        state.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("status"));
        notes.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("notes"));

        final TreeItem<Order> root = new RecursiveTreeItem<>(ordersData, RecursiveTreeObject::getChildren);

        customerOrdersTreeTable.setRoot(root);
        customerOrdersTreeTable.setOnMouseClicked(event -> {
            if(event.getClickCount() == 2) {
                try {
                    selectedOrder = customerOrdersTreeTable.getSelectionModel().getSelectedItem().getValue();
                    orderDetailController = new CustomersOrderDetailController();
                    openNewAnchorPage(anchorPane, "../"+path+"/customerorders/customers_orders_detail.fxml", orderDetailController);
                    orderDetailController.init(this);
                } catch(Exception e) {}

            }
        });
        customerOrdersTreeTable.setShowRoot(false);
    }
}
