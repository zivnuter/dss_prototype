package main.presentation.java.content.orders.toexecute;

import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import main.model.Order;
import main.presentation.java.CommonController;
import main.presentation.java.content.orders.OrdersController;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * This controller handles orders to execute tab pane in orders. It shows the order overview.
 */
public class OrdersToExecuteController extends OrdersController {
    OrdersController ordersController;
    private OrdersToExecuteDetailController detailController;
    protected static Order selectedOrder = null;

    @FXML
    private JFXTreeTableView<Order> ordersToExecuteTreeTable;
    @FXML
    private ObservableList<Order> ordersData;
    @FXML
    private AnchorPane anchorPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setTreeTable();
    }

    public void init(OrdersController controller) { ordersController = controller; }

    /**
     * This method handles settings button. Its not implemented yet.
     * @param mouseEvent
     */
    public void handleSettingsButton(MouseEvent mouseEvent) {
//        openNewStackPage(stackPane, pathFromProfileToCustomerPackage+"/customers.fxml");
    }

    public void reloadTreeTable() {
        ordersToExecuteTreeTable.getColumns().clear();
        setTreeTable();
    }

    public void reloadCustomersOrders() {
        ordersController.reloadCustomersOrders();
    }

    private void setTreeTable() {
        TreeTableColumn id = new TreeTableColumn("ID");
        id.setMinWidth(50);
        TreeTableColumn customer = new TreeTableColumn("Zákazník");
        customer.setMinWidth(200);
        TreeTableColumn price = new TreeTableColumn("Cena");
        price.setMinWidth(150);
        TreeTableColumn state = new TreeTableColumn("Stav");
        state.setMinWidth(100);
        TreeTableColumn notes = new TreeTableColumn("Poznámky");
        notes.setMinWidth(250);
        ordersToExecuteTreeTable.getColumns().addAll(id, customer, price, state, notes);

        try {
            ordersData = data.handleOrder.getOrdersToExecute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        id.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("id"));
        customer.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("customer"));
        price.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("price"));
        state.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("status"));
        notes.setCellValueFactory(new TreeItemPropertyValueFactory<Order,String>("notes"));

        final TreeItem<Order> root = new RecursiveTreeItem<>(ordersData, RecursiveTreeObject::getChildren);

        ordersToExecuteTreeTable.setRoot(root);
        ordersToExecuteTreeTable.setOnMouseClicked(event -> {
            if(event.getClickCount() == 2) {
                try {
                    selectedOrder = ordersToExecuteTreeTable.getSelectionModel().getSelectedItem().getValue();
                    detailController = new OrdersToExecuteDetailController();
                    openNewAnchorPage(anchorPane, "../"+path+"/toexecute/to_execute_detail.fxml", detailController);
                    detailController.init(this);
                } catch(Exception e) {}
            }
        });
        ordersToExecuteTreeTable.setShowRoot(false);
    }
}
