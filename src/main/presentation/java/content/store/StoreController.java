package main.presentation.java.content.store;

import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import main.presentation.java.CommonController;
import main.model.Product;
import main.model.Store;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.*;

/**
 * This controller handles store page generally. It shows the product overview.
 */
public class StoreController extends CommonController {
    static final String path = "../../" + pathToFxmlPackage + "/content/store";

    static final String NAME = "Název produktu";
    static final String PRICE = "Cena";
    static final String COUNT = "Počet kusů";
    static final String ENDURANCE = "Průměrná výdrž";
    static final String NOTES = "Poznámky";


    private Store store;
    static Product selectedProduct = null;
    @FXML
    private StackPane stackPane;
    @FXML
    private JFXTreeTableView<Product> productsTreeTable;
    @FXML
    private ObservableList<Product> productsData;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            store = data.handleStore.getAllStores();
        } catch (SQLException e) {
            openUnderstandDialogWithText(stackPane, STORE_ERROR);
            e.printStackTrace();
        }
        loadProducts();
    }

    /**
     * This method handles settings button. Its not implemented yet.
     * @param mouseEvent
     */
    public void handleSettingsButtonAction(MouseEvent mouseEvent) { }

    /**
     * This method handles add button. It opens add product page.
     * @param mouseEvent
     */
    public void handleAddButtonAction(MouseEvent mouseEvent) {
        openNewStackPage(stackPane, path + "/store_create.fxml");
    }

    private void loadProducts() {
        TreeTableColumn id = new TreeTableColumn("ID");
        id.setMinWidth(50);
        TreeTableColumn productName = new TreeTableColumn("Název produktu");
        productName.setMinWidth(250);
        TreeTableColumn price = new TreeTableColumn("Cena");
        price.setMinWidth(100);
        TreeTableColumn count = new TreeTableColumn("Počet ks");
        count.setMinWidth(100);
        TreeTableColumn notes = new TreeTableColumn("Poznámky");
        notes.setMinWidth(230);

        productsTreeTable.getColumns().addAll(id, productName, price, count, notes);

        try {
            productsData = data.handleProduct.getAllProducts();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        id.setCellValueFactory(new TreeItemPropertyValueFactory<Product,String>("id"));
        productName.setCellValueFactory(new TreeItemPropertyValueFactory<Product,String>("name"));
        price.setCellValueFactory(new TreeItemPropertyValueFactory<Product,String>("price"));
        count.setCellValueFactory(new TreeItemPropertyValueFactory<Product,String>("count"));
        notes.setCellValueFactory(new TreeItemPropertyValueFactory<Product,String>("notes"));

        final TreeItem<Product> root = new RecursiveTreeItem<>(productsData, RecursiveTreeObject::getChildren);

        productsTreeTable.setRoot(root);
        productsTreeTable.setOnMouseClicked(event -> {
            if(event.getClickCount() == 2) {
                selectedProduct = productsTreeTable.getSelectionModel().getSelectedItem().getValue();
                openNewStackPage(stackPane, path+"/store_detail.fxml");
            }
        });
        productsTreeTable.setShowRoot(false);
    }

    List<HBox> createFields(ScrollPane scrollPane) {
        VBox content = new VBox();
        List<HBox> infoLines = new ArrayList<>();
        addHboxWith(NAME+"*:", "textfield", content, infoLines);
        addHboxWith(PRICE+"*:", "textfield", content, infoLines);
        addHboxWith(COUNT+"*:", "textfield", content, infoLines);
        addHboxWith(ENDURANCE+":", "textfield", content, infoLines);
        addHboxWith(NOTES+":", "textarea", content, infoLines);
        scrollPane.setContent(content);
        return infoLines;
    }

    Product createProduct(List<HBox> infoLines) {
        String name = "", notes = "";
        double price = Double.MIN_VALUE, endurance = 0;
        int count = Integer.MIN_VALUE;

        for (HBox hbox : infoLines) {
            Node labelNode = hbox.getChildren().get(0);
            Node fieldNode = hbox.getChildren().get(1);
            String labelText = "";

            if(labelNode instanceof Label) { labelText = ((Label)labelNode).getText(); }
            if(labelText.contains(NAME)) {
                if(fieldNode instanceof JFXTextField) { name = ((JFXTextField)fieldNode).getText(); }
            } else if(labelText.contains(PRICE)) {
                if(fieldNode instanceof JFXTextField) {
                    try { price = Double.parseDouble(((JFXTextField) fieldNode).getText()); }
                    catch(Exception e) { return null; }
                }
            } else if(labelText.contains(COUNT)) {
                if(fieldNode instanceof JFXTextField) {
                    try { count = Integer.parseInt(((JFXTextField)fieldNode).getText()); }
                    catch(Exception e) { return null; }
                }
            } else if(labelText.contains(ENDURANCE)) {
                if(fieldNode instanceof JFXTextField) {
                    String text = ((JFXTextField)fieldNode).getText();
                    if(!text.equals("")) {
                        try { endurance = Double.parseDouble(text); }
                        catch(Exception e) { return null; }
                    }
                }
            } else if(labelText.contains(NOTES)) {
                if (fieldNode instanceof JFXTextArea) {
                    notes = ((JFXTextArea) fieldNode).getText();
                }
            }
        }
        return new Product(0, name, price, count, endurance, notes);
    }
}
