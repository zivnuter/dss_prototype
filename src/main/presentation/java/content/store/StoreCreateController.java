package main.presentation.java.content.store;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import main.model.Product;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.*;

/**
 * This controller handles add product page.
 */
public class StoreCreateController extends StoreController {
    private List<HBox> infoLines;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private StackPane stackPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        infoLines = createFields(scrollPane);
    }

    /**
     * This method handles create button. It creates the product.
     * @param mouseEvent
     */
    public void create(MouseEvent mouseEvent) {
        if(!checkMandatoryFields(stackPane, infoLines)) { return; }
        try {
            Product p = createProduct(infoLines);
            if(p == null) {
                openUnderstandDialogWithText(stackPane, DIALOG_FILL_NUMBERS);
                return;
            }
            data.handleProduct.insertProduct(p);
            openNewStackPage(stackPane, path + "/store.fxml");
            openUnderstandDialogWithText(stackPane, STORE_CREATE_DIALOG_OK);
        } catch (SQLException e) {
            openUnderstandDialogWithText(stackPane, DIALOG_ERROR);
            e.printStackTrace();
        }
    }

    /**
     * This method handles back button. It returns user to the overiew.
     * @param mouseEvent
     */
    public void back(MouseEvent mouseEvent) {
        openBackYesNoDialogWithText(STORE_CREATE_DIALOG_THROW);
    }

    private void openBackYesNoDialogWithText(String withBody) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setBody(new Text(withBody));

        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button1 = new JFXButton(DIALOG_YES);
        JFXButton button2 = new JFXButton(DIALOG_NO);
        button1.setOnAction(event -> {
            dialog.close();
            openNewStackPage(stackPane, path+"/store.fxml");
        });
        button2.setOnAction(event -> dialog.close());
        content.setActions(button1,button2);
        dialog.show();
    }
}
