package main.presentation.java.content.store;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import main.model.Customer;
import main.model.Product;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import static main.constants.DialogContants.*;
import static main.constants.GeneralConstants.STRING_FALSE;
import static main.constants.GeneralConstants.STRING_TRUE;

/**
 * This controller handles product detail page.
 */
public class StoreDetailController extends StoreController {
    private List<HBox> infoLines;
    @FXML
    private Label nameLabel;
    @FXML
    private StackPane stackPane;
    @FXML
    private ScrollPane scrollPane;

    /**
     * This method loads existing info about selected product.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nameLabel.setText(selectedProduct.getName());
        infoLines = createFields(scrollPane);
        loadInfo();
    }

    /**
     * This method handles back button. It returns user to the overview.
     * @param mouseEvent
     */
    public void handleBackButton(MouseEvent mouseEvent) {
        openNewStackPage(stackPane, path+"/store.fxml");
    }

    /**
     * This method handles save button. It updates existing product.
     * @param mouseEvent
     */
    public void save(MouseEvent mouseEvent) {
        if(!checkMandatoryFields(stackPane, infoLines)) { return; }
        Product p = createProduct(infoLines);
        if(p == null) {
            openUnderstandDialogWithText(stackPane, DIALOG_FILL_NUMBERS);
            return;
        }
        p.setId(selectedProduct.getId());
        if(p.equals(selectedProduct)) { openUnderstandDialogWithText(stackPane, DIALOG_NO_CHANGES); }
        else {
            openUpdateYesNoDialogWithText(STORE_UPDATE_DIALOG, p);
        }
    }

    /**
     * This method handles delete button. It deletes the selected product.
     * @param mouseEvent
     */
    public void delete(MouseEvent mouseEvent) {
        openDeleteYesNoDialogWithText(STORE_DELETE_DIALOG);
    }

    private void loadInfo() {
        for (HBox hbox : infoLines) {
            loadLine(hbox);
        }
    }

    private void loadLine(HBox hbox) {
        Node labelNode = hbox.getChildren().get(0);
        Node fieldNode = hbox.getChildren().get(1);
        String labelText = "";

        if(labelNode instanceof Label) { labelText = ((Label)labelNode).getText(); }
        if(labelText.contains(NAME)) {
            if(fieldNode instanceof JFXTextField) { ((JFXTextField)fieldNode).setText(selectedProduct.getName()); }
        } else if(labelText.contains(PRICE)) {
            if(fieldNode instanceof JFXTextField) { ((JFXTextField)fieldNode).setText(Double.toString(selectedProduct.getPrice())); }
        } else if(labelText.contains(COUNT)) {
            if(fieldNode instanceof JFXTextField) { ((JFXTextField)fieldNode).setText(Integer.toString(selectedProduct.getCount())); }
        } else if(labelText.contains(ENDURANCE)) {
            if(fieldNode instanceof JFXTextField) {
                if(selectedProduct.getEndurance() > 0) {
                    ((JFXTextField)fieldNode).setText(Double.toString(selectedProduct.getEndurance()));
                }
            }
        } else if(labelText.contains(NOTES)) {
            if(fieldNode instanceof JFXTextArea) { ((JFXTextArea)fieldNode).setText(selectedProduct.getNotes()); }
        }
    }

    private void updateProduct(Product product) {
        try {
            Product changes = getChanges(product);
            data.handleProduct.updateProduct(product, changes);
            selectedProduct = product;
            openUnderstandDialogWithText(stackPane, DIALOG_UPDATE_OK);
        } catch (SQLException e) {
            openUnderstandDialogWithText(stackPane, DIALOG_ERROR);
            e.printStackTrace();
        }
    }

    private Product getChanges(Product product) {
        String name = STRING_FALSE, notes = STRING_FALSE;
        int price = Integer.MIN_VALUE, count = Integer.MIN_VALUE, endurance = Integer.MIN_VALUE;
        if(!selectedProduct.getName().equals(product.getName())) { name = STRING_TRUE; }
        if(selectedProduct.getPrice() != product.getPrice()) { price = Integer.MAX_VALUE; }
        if(selectedProduct.getCount() != product.getCount()) { count = Integer.MAX_VALUE; }
        if(selectedProduct.getEndurance() != product.getEndurance()) { endurance = Integer.MAX_VALUE; }
        if(!selectedProduct.getNotes().equals(product.getNotes())) {
            notes = STRING_TRUE;
        }
        return new Product(0, name, price, count, endurance, notes);
    }

    private void deleteProduct() {
        try {
            data.handleProduct.deleteProduct(selectedProduct.getId());
        } catch (SQLException e) {
            openUnderstandDialogWithText(stackPane, DIALOG_ERROR);
            e.printStackTrace();
        }
    }

    private void openDeleteYesNoDialogWithText(String withBody) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setBody(new Text(withBody));

        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button1 = new JFXButton(DIALOG_YES);
        JFXButton button2 = new JFXButton(DIALOG_NO);
        button1.setOnAction(event -> {
            dialog.close();
            deleteProduct();
            openNewStackPage(stackPane, path+"/store.fxml");
            openUnderstandDialogWithText(stackPane, STORE_DELETE_DIALOG_OK);
        });
        button2.setOnAction(event -> dialog.close());
        content.setActions(button1,button2);
        dialog.show();
    }

    private void openUpdateYesNoDialogWithText(String withBody, Product product) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.setBody(new Text(withBody));

        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton button1 = new JFXButton(DIALOG_YES);
        JFXButton button2 = new JFXButton(DIALOG_NO);
        button1.setOnAction(event -> {
            dialog.close();
            updateProduct(product);
        });
        button2.setOnAction(event -> dialog.close());
        content.setActions(button1,button2);
        dialog.show();
    }
}
