package main.presentation.java.login;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import main.presentation.java.CommonController;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import static main.constants.DialogContants.*;

/**
 * This controller handles create login page.
 */
public class CreateLoginController extends CommonController {
    @FXML
    private StackPane stackPane;
    @FXML
    private JFXTextField login;
    @FXML
    private JFXPasswordField pass;

    @Override
    public void initialize(URL url, ResourceBundle rb) { }

    /**
     * This method creates login and initialize the database.
     * @param actionEvent
     */
    public void handleCreateLoginButtonAction(ActionEvent actionEvent) {
        if(login.getText().equals("") || pass.getText().equals("")) {
            openUnderstandDialogWithText(stackPane, LOGIN_DIALOG_CREATE_MANDATORY);
        } else if(!checkEmail(login.getText())) {
            openUnderstandDialogWithText(stackPane, LOGIN_DIALOG_CREATE_EMAIL);
        } else {
            try {
                data.handleLogin.createLogin(login.getText(), pass.getText());
                data.handleStore.createStore("Hlavní", 1);
                String[] attr = {"E-mail", "Věk"};
                data.handleCustomersAttribute.createAttributes(attr);
                String[] reasons = {"Narozeniny", "Kontrola", "Kontaktovat"};
                data.handleReason.insertReasons(reasons);
                openNewStackPage(stackPane, "../"+pathToFxmlPackage+"/login/login.fxml");
                openUnderstandDialogWithText(stackPane, LOGIN_DIALOG_CREATE_OK);
            } catch (SQLException e) {
                openUnderstandDialogWithText(stackPane, DIALOG_ERROR);
                e.printStackTrace();
            }
        }
    }
}
