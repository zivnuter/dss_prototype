package main.presentation.java.login;

import com.jfoenix.controls.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import main.presentation.java.CommonController;
import main.model.Login;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import static main.constants.DialogContants.LOGIN_DIALOG_WARNING;

/**
 * This controller handles login page.
 */
public class LoginController extends CommonController {
    private Login loginData = null;
    @FXML
    private JFXTextField login;
    @FXML
    private JFXPasswordField pass;
    @FXML
    private StackPane stackPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            loginData = data.handleLogin.getLoginData();
            if(loginData.getId() == 0) {
                openNewStackPage(stackPane, "../"+pathToFxmlPackage+"/login/login_create.fxml");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method handles login button and opens the application.
     * @param event
     * @throws IOException
     */
    @FXML
    private void handleLoginButtonAction(ActionEvent event) throws IOException {
        if(loginData.getEmail().equals(login.getText()) && loginData.getPassword().equals(pass.getText())) {
            loadApplication(event);
        } else {
            openUnderstandDialogWithText(stackPane, LOGIN_DIALOG_WARNING);
        }
    }

    private void loadApplication(ActionEvent event) throws IOException {
        Parent app_parent = FXMLLoader.load(
                getClass().getResource("../"+pathToFxmlPackage+"/sidebar.fxml")
        );
        Scene app_scene = new Scene(app_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(app_scene);
        app_stage.show();
    }
}
